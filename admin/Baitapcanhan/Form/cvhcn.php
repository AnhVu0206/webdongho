<!DOCTYPE HTML>
<html lang="en">
    <head>
        <title>Chu vi HCN</title>
        <!-- <link rel="stylesheet" href="style.css"> -->
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    </head>
    <body>
        <?php
        if(isset($_POST["submit"]))
        {
            $Width=$_POST["width"];
            $Height=$_POST["height"];
            if($Width<=0||$Height<=0) $Perimeter="nhập sai, nhập lại";
            else $Perimeter=($Width+$Height)*2;
        }
        ?>
        <form action="" method="post">
            <table bgcolor="#faebd" align="center">
                <tr>
                    <td align="center" bgcolor="orange" colspan="2" >
                        <h2>Chu vi hình chữ nhật</h2>
                    </td>
                </tr>
                <tr>
                    <td>Width</td>
                    <td>
                        <input Type="text" size="30" name="width" placeholder="nhập chiều dài"
                        value="<?php if(isset($_POST['width'])) echo $_POST['width'];?>"
                        >
                    </td>
                </tr>
                <tr>
                    <td>Height</td>
                    <td>
                        <input Type="text" size="30" name="height" placeholder="nhập chiều rộng"
                        value="<?php if(isset($_POST['height'])) echo $_POST['height'];?>"
                        >
                    </td>
                </tr>
                <tr>
                    <td>Perimeter</td>
                    <td>
                        <input Type="text" size="30" placeholder="kết quả" readonly value="<?php echo "$Perimeter"?>">
                        
                    </td>
                </tr>
                <tr>
                    <td colspan="2" align="center">
                        <input type="submit" value="tính" name="submit">
                    </td>
                </tr>
            </table>
        </form>
    </body>
</html>