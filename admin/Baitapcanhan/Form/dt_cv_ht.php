
<!DOCTYPE HTML>
<html lang="en">
    <head>
        <title>Chu vi và diện tích hình tròn</title>
        <!-- <link rel="stylesheet" href="style.css"> -->
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    </head>
    <body>
        <?php
        $Perimeter=0;
        $Area=0;
        if(isset($_POST["submit"]))
        {
            $bk=$_POST["bk"];
            
            if($bk<=0)
            {
                $Perimeter="nhập sai, nhập lại";
                $Area="nhập sai, nhập lại";
            }
            else
            {
                $Perimeter= $bk*2*3.14;
                $Area= $bk*$bk*3.14;
            }
            
        }
        ?>
        <form action="" method="post">
            <table bgcolor="#faebd" align="center">
                <tr>
                    <td align="center" bgcolor="orange" colspan="2" >
                        <h2>Chu vi và diện tích hình tròn</h2>
                    </td>
                </tr>
                <tr>
                    <td>Bán kính</td>
                    <td>
                        <input Type="text" size="30" name="bk" placeholder="nhập bán kính"
                        value="<?php if(isset($_POST['bk'])) echo $_POST['bk'];?>"
                        >
                    </td>
                </tr>
                <tr>
                    <td>Perimeter</td>
                    <td>
                        <input Type="text" size="30"  placeholder="kết quả" readonly value="<?php echo "$Perimeter cm";?>">
                    </td>
                </tr>
                <tr>
                    <td>Area</td>
                    <td>
                        <input Type="text" size="30" placeholder="kết quả" readonly value="<?php echo "$Area cm2";?>">
                        
                    </td>
                </tr>
                <tr>
                    <td colspan="2" align="center">
                        <input type="submit" value="tính" name="submit">
                    </td>
                </tr>
            </table>
        </form>
    </body>
</html>