<!DOCTYPE HTML>
<html lang="en">
    <head>
        <title>Xét tốt nghiệp</title>
        <!-- <link rel="stylesheet" href="style.css"> -->
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    </head>
    <body>
        <?php
        $diemchuan=20;
        $tongdiem=0;
        $ketqua=" ";
        if(isset($_POST['submit']))
        {
            $toan=$_POST['toan'];
            $ly=$_POST['ly'];
            $hoa=$_POST['hoa'];
            $tongdiem=($toan+$ly+$hoa);
            
            if($toan<0 || $toan>10 || $ly<0 || $ly>10 || $hoa<0 || $hoa>10)
            {
                $ketqua="nhập sai, nhập lại";
            }
            else
            {
                if($toan == 0 || $ly== 0 || $hoa== 0)
                {
                    $ketqua="Rớt";
                }
                else 
                {
                    if($tongdiem >= $diemchuan) $ketqua= "Đậu";
                    else $ketqua="Rớt";
                }
            }
        }
        ?>

        <form action="" method="POST">
        <table bgcolor="#DDA0DD" align="center">
            <tr>
                <td colspan="3" bgcolor="#FF1493" align="center">
                    <h2>Kết quả thi đại học</h2>
                </td>
            </tr>
            <tr>
                <td>Toán</td>
                <td>
                    <input type="text" name="toan" placeholder="nhập điểm toán" size ="30"
                    value ="<?php if(isset($_POST['toan'])) echo$_POST['toan']; ?>"
                    >
                </td>
                <td>&emsp;</td>
            </tr>
            <tr>
                <td>Lý</td>
                <td>
                    <input type="text" name="ly" placeholder="nhập điểm lý" size ="30"
                    value ="<?php if(isset($_POST['ly'])) echo$_POST['ly']; ?>"
                    >
                </td>
                <td>&emsp;</td>
            </tr>
            <tr>
                <td>Hoá</td>
                <td>
                    <input type="text" name="hoa" placeholder="nhập điểm hoá" size ="30"
                    value ="<?php if(isset($_POST['hoa'])) echo$_POST['hoa']; ?>"
                    >
                </td>
                <td>&emsp;</td>
            </tr>
            <tr>
                <td>Điểm chuẩn  &ensp;</td>
                <td>
                    <input type="text" name="diemchuan" size ="30"
                    value ="<?php echo $diemchuan ?>"
                    >
                </td>
                <td>&emsp;</td>
            </tr><tr>
                <td>Tổng điểm</td>
                <td>
                    <input type="text" name="tongdiem" size ="30"
                    value ="<?php echo $tongdiem ?>"
                    >
                </td>
                <td>&emsp;</td>
            </tr>
            <tr>
                <td>Kết quả thi</td>
                <td>
                    <input type="text" name="ketqua" size ="30" readonly value="<?php echo $ketqua ?>">
                </td>
                <td>&emsp;</td>
            </tr>
            <tr>
                <td colspan="2" align="center">
                    <input type="submit" name="submit" value="Xem kết quả">
                </td>
            </tr>
        </table>
        </form>
    </body>
</html> 