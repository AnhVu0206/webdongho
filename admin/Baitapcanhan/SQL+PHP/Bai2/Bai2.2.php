<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>2.2</title>
    <style>
        #th
        {
            color:red;
        }
    </style>
</head>
<body>
    <h3 style="text-transform: uppercase" align="center">Thông tin khách hàng</h3>
    <table align="center" border="true">
        <tr id="th">
            <th>Mã Khách Hàng</th>
            <th>Tên Khách Hàng</th>
            <th>Giới tính</th>
            <th>Địa chỉ</th>
            <th>Điện thoại</th>
            <th>Email</th>
        </tr>
        <?php
            require("./config.php");
            $query='select * from khach_hang';
            $result=$conn->query($query);
            if (!$result) echo "cau truy van bi sai";
            if ($result->num_rows != 0){
                $dem=0;
                while ($row=$result->fetch_array()){
                    if($dem%2==0) echo"<tr bgcolor='yellow'>";
                    else echo"<tr>";
                    for($i=0;$i<$result->field_count;$i++)
                    {
                        if($i==2)
                        {
                            // if($row[$i]==1) echo"<td align='center'>Nam</td>";
                            // else echo"<td align='center'>Nữ</td>";
                            if($row[$i]==1) echo"<td align='center'><img width='50px' height='50px' src='nam.jpg'></td>";
                            else echo"<td align='center'><img width='50px' height='50px' src='nu.jpg'></td>";
                        }
                        else echo"<td>".$row[$i]."</td>";
                    }
                    $dem++;
                }
            } else echo "bang khong co du lieu";
            $conn->close();
        ?>
    </table>
</body>
</html>