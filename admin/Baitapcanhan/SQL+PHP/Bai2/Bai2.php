<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>2.1</title>
</head>
<body>
    <table align="center" border="true">
        <tr>
            <th>Mã HS</th>
            <th>Tên hãng sữa</th>
            <th>Địa chỉ</th>
            <th>Điện thoại</th>
            <th>Email</th>
        </tr>
        <?php
            require("config.php");
            $query='select * from hang_sua';
            $result=$conn->query($query);
            if (!$result) echo "cau truy van bi sai";
            if ($result->num_rows != 0){
                while ($row=$result->fetch_array()){//row la 1 mang
                    echo "<tr>";
                    echo "<td>" . $row['Ma_hang_sua'] . "</td>";
                    echo "<td>" . $row['Ten_hang_sua'] . "</td>";
                    echo "<td>" . $row['Dia_chi'] . "</td>";
                    echo "<td>" . $row['Dien_thoai'] . "</td>";
                    echo "<td>" . $row['Email'] . "</td>";
                    echo "</tr>";
                }
            } else echo "bang khong co du lieu";
            $conn->close();
        ?>
    </table>
</body>
</html>