<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Bài 2</title>
    <style>
        #box{
            border-width: 2px;
            border-style: solid;
            border-color: grey;
            box-shadow: 5px 5px 5px black;
        }
        #submit{
            text-align: center;
            background-color:#EEE8AA;
            width: 60%;
        }
    </style>
</head>
<body>
<?php
function ktso($arr)
{
    foreach ($arr as $i)
    {
        if (is_numeric($i)== FALSE) return FALSE;
    }
    return TRUE;
}
function Tong($arr)
{
    $t=0;
    for($i=0;$i<count($arr);$i++)
    {
        $t=$t+$arr[$i];
    }
    return $t;
}
if(isset($_POST["submit"]))
{
    $arr=[];
    $m=$_POST["m"];
    $original=explode(",",$m);
    if(ktso($original)==TRUE)
    {
        $mang= implode(", ",$original);
        $kq=Tong($original);
    }
    else $kq = "Hãy nhập số ";
}
?>
    <form action="" method="post">
        <table  align="center" bgcolor="#AFEEEE" id="box">
            <tr>
                <td colspan="4" align="center" bgcolor="#008B8B">
                    <h2 style="color:white">Nhập mảng và tính trên dãy số</h2>
                </td>
            </tr>
            <tr>
                <td>Nhập dãy số: &emsp; &emsp;</td>
                <td>
                    <input type="text" name="m" size="30"
                    value ="<?php if(isset($_POST['m'])) echo$_POST['m']; ?>"
                    >
                </td>
                <td><span style="color:red">(*)&emsp; &emsp;</span></td>
            </tr>
            <tr>
                <td></td>
                <td >
                    <input type="submit" name="submit" id="submit" value="Tổng dãy số"> 
                </td>
            </tr>
            <tr>
                <td>Tổng dãy số:</td>
                <td>
                    <input type="text" name="kq" size="30" value="<?php if(isset($_POST['kq'])) echo $kq;?>">
                </td>
                <td>&emsp;</td>
            </tr>
            <tr>
                <td colspan="3" align="center" >
                    <span><span style="color:red">(*)</span> Các số được nhập sẽ cách nhau bằng dấu ";"</span>
                </td>
            </tr>
        </table>
    </form>
</body>
</html>