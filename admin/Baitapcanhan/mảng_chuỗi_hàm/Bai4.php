<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Bài 4</title>
    <style>
        #box{
            border-width: 2px;
            border-style: solid;
            border-color: grey;
            box-shadow: 5px 5px 5px black;
        }
        #submit{
            text-align: center;
            background-color: #87CEFA;
            width: 30%;
        }
    </style>
</head>
<body>
<?php
function ktso($arr)
{
    foreach($arr as $i)
    {
        if(is_numeric($i)==False) return False;
    }
    return True;
}
function Timkiem($arr,$n)
{
    $vitri=0;
    for($i=0;$i<count($arr);$i++)
    {
        if($n==$arr[$i])
        {
            $vitri=$i+1;        
            return "Tìm thấy $n tại vị trí thứ $vitri của mảng";
        }
    }
    return "Không tìm thấy số $n trong mảng";
}
if(isset($_POST["submit"]))
{
    $arr=[];
    $m=$_POST["m"];
    $n=$_POST["n"];
    $original=explode(",",$m);
    if(ktso($arr)==True)
    {
        $mang= implode(", ",$original);
        $kq=Timkiem($original,$n);
    }
    else $kq = "Nhập sai, nhập lại";
}
?>
    <form action="" method="post">
        <table  align="center" bgcolor="#AFEEEE" id="box">
            <tr>
                <td colspan="4" align="center" bgcolor="#008B8B">
                    <h2 style="color:white">Tìm kiếm</h2>
                </td>
            </tr>
            <tr>
                <td>Nhập mảng:</td>
                <td>
                    <input type="text" name="m" size="40"
                    value ="<?php if(isset($_POST['m'])) echo$_POST['m']; ?>"
                    >
                </td>
                <td></td>
            </tr>
            <tr>
                <td>Nhập số cần tìm:</td>
                <td>
                    <input type="text" name="n" size="8" value="<?php if(isset($_POST['n'])) echo $n;?>">
                </td>
                <td>&emsp;</td>
            </tr>
            <tr>
                <td></td>
                <td >
                    <input type="submit" name="submit" id="submit" value="Tìm kiếm"> 
                </td>
            </tr>
            <tr>
                <td>Mảng:</td>
                <td>
                    <input type="text" name="mang" size="40" value="<?php if(isset($_POST['mang'])) echo $mang;?>">
                </td>
                <td>&emsp;</td>
            </tr>
            <tr>
                <td>Kết quả tìm kiếm:</td>
                <td>
                    <input type="text" name="kq" size="40" value="<?php if(isset($_POST['kq'])) echo $kq;?>">
                </td>
                <td>&emsp;</td>
            </tr>
            <tr bgcolor="#008B8B">
                <td colspan="3" align="center" >
                    <span>(Các phần tử trong mảng sẽ cách nhau bằng dấu ";")</span>
                </td>
            </tr>
        </table>
    </form>
</body>
</html>