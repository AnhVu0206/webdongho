<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Bài 5</title>
    <style>
        #box{
            border-width: 2px;
            border-style: solid;
            border-color: grey;
            box-shadow: 5px 5px 5px black;
        }
        #submit{
            text-align: center;
            background-color: #F0E68C;
            width: 30%;
        }
    </style>
</head>
<body>
    <?php
    function thaythe($arr,$a,$b)
    {
        for($i=0;$i<count($arr);$i++)
        {
            if($arr[$i]==$a) $arr[$i]=$b;
        }
        return $arr;
    }
    if (isset($_POST['submit'])){
        $m=$_POST['m'];
        $a=$_POST['a'];
        $b=$_POST['b'];
        $original=explode(",",$m);
        $mangcu=implode(",",$original);
        $mangmoi=implode(",",thaythe($original,$a,$b));
    }
    ?>
    <form action="" method="Post">
        <table align="center" bgcolor="#AFEEEE" id="box">
            <tr>
                <td bgcolor="#008B8B" align="center" colspan="3">
                    <h2 style="color:white">Thay Thế</h2>
                </td>
            </tr>
            <tr>
                <td>Nhập các phần tử: </td>
                <td>
                    <input type="text" name="m" value="<?php if (isset($m)) echo $m;?>"  size="40">
                </td>
                <td> &emsp;</td>
            </tr>
            <tr>
                <td>Giá trị cần thay thế:&emsp;</td>
                <td>
                    <input type="text" name="a" size="30" value="<?php if (isset($a)) echo $a;?>">
                </td>
            </tr>
            <tr>
                <td>Giá trị thay thế:</td>
                <td>
                    <input type="text" name="b" size="30" value="<?php if (isset($b)) echo $b;?>">
                </td>
            </tr>
            <tr>
                <td></td>
                <td>
                    <input type="submit" name="submit" id="submit" value ="Thay thế">
                </td>
            </tr>
            <tr>
                <td>Mảng cũ:</td>
                <td>
                    <input type="text" size="40" value="<?php if (isset($mangcu)) echo $mangcu;?>">
                </td>
            </tr>
            <tr>
                <td>Mảng mới:</td>
                <td>
                    <input type="text" size="40" value="<?php if (isset($mangmoi)) echo $mangmoi;?>">
                </td>
            </tr>
            <tr>
                <td colspan="3" align="center">
                    <span>(<span style="color:red">Ghi chú:</span> Các phần tử trong mảng cách nhau bằng dấu ",")</span>
                </td>
            </tr>
        </table>
    </form>
</body>
</html>