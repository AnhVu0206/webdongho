<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Bài 6</title>
    <style>
        #box{
            border-width: 2px;
            border-style: solid;
            border-color: grey;
            box-shadow: 5px 5px 5px black;
        }
    </style>
</head>
<body>
    <?php
    function doicho(&$a,&$b)
    {
        $tam=$a;
        $a=$b;
        $b=$tam;    
    }
    function sapxeptang($arr)
    {
        for($i=0; $i<count($arr)-1;$i++)
        {
            for($j=$i+1;$j<count($arr);$j++)
            {
                if($arr[$i]>$arr[$j]) doicho($arr[$i],$arr[$j]);

            }
        }
        return $arr;
    }
    function sapxepgiam($arr)
    {
        for($i=0; $i<count($arr)-1;$i++)
        {
            for($j=$i+1;$j<count($arr);$j++)
            {
                if($arr[$i]<$arr[$j]) doicho($arr[$i],$arr[$j]);
            }
        }
        return $arr;
    }
    if (isset($_POST['submit'])){
        $text=$_POST['text'];
        $original=explode(",",$text);
        $result_decre = implode(",",sapxepgiam($original));
        $result_incre = implode(",",sapxeptang($original));
    }
    ?>
    <form action="" method="Post">
        <table align="center" bgcolor="#AFEEEE" id="box">
            <tr>
                <td bgcolor="#008B8B" align="center" colspan="3">
                    <h2 style="color:white">Sắp xếp mảng</h2>
                </td>
            </tr>
            <tr>
                <td>Nhập mảng:</td>
                <td>
                    <input type="text" name="text" value="<?php if (isset($text)) echo $text;?>"  size="30">
                </td>
                <td style="color: red">(*)</td>
            </tr>
            <tr>
                <td></td>
                <td>
                    <input type="submit" name="submit" value ="Sắp xếp tăng/giảm">
                </td>
            </tr>
            <tr>
                <td>Sau khi sắp xếp:</td>
            </tr>
            <tr>
                <td>Tăng dần:</td>
                <td>
                    <input type="text" value="<?php if(isset($result_incre)) echo  $result_incre;?>">
                </td>
            </tr>
            <tr>
                <td>Giảm dần:</td>
                <td>
                    <input type="text" value="<?php if(isset($result_decre)) echo  $result_decre;?>">
                </td>
            </tr>
            <tr>
                <td colspan="3" align="center">
                    <span><span style="color:red">(*)</span> Các số được nhập cách nhau bằng dấu ","</span>
                </td>
            </tr>
        </table>
    </form>
</body>
</html>