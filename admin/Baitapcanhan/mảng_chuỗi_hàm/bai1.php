<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Form</title>
    <style>
        #box{
            border-width: 2px;
            border-style: solid;
            border-color: grey;
            box-shadow: 5px 5px 5px black;
        }
    </style>
</head>
<body>
    <?php
    function tao_mang(&$n,&$arr)
    {
        for($i=0;$i<$n;$i++)
        {
            $x=rand(-100,200);
            $arr[$i]=$x;
        }
    }
    function Demsochan($arr)
    {
        $dem=0;
        foreach($arr as $i)
        {
            if($i%2==0) $dem++;
        }
        return $dem;
    }
    function Demsobehon100($arr)
    {
        $dem=0;
        foreach($arr as $i)
        {
            if($i<100) $dem++;
        }
        return $dem;
    }
    function Tongsoam($arr)
    {
        $t=0;
        foreach ($arr as $i)
        {
            if($i<0) $t=$t+$i;
        }
        return $t;
    }
    function invitri0($arr)
    {
        $index="0";
        foreach ($arr as $key=>$value)
        {
            if($value==0) $index=$key+1;
        }
        if($index==0) return "Trong mảng không có số 0 nào";
        else return "Vị trí của phần tử mang giá trị âm là $index";
    }
    function sapxeptang($n,&$arr)
    {
        for($i=0; $i<$n-1;$i++)
        {
            for($j=$i+1;$j<$n;$j++)
            {
                if($arr[$i]>$arr[$j])
                {
                    $tg=$arr[$i];
                    $arr[$i]=$arr[$j];
                    $arr[$j]=$tg;
                }
            }
        }
    }
    function xuatmang($arr)
    {
        $m="";
        foreach($arr as $i)
        {
            $m.= $i." ";
        }
        return $m;
    }
    if(isset($_POST['submit']))
    {
        $n=$_POST['input'];
        if($n>0 && is_numeric($n) && is_int($n+0))
        {
            tao_mang($n,$arr);
            $kq="Mảng được tạo là:\n" .implode(",",$arr)."\n";
            $kq.="Có ".Demsochan($arr)." số chẳn trong mảng\n";
            $kq.="Có ".Demsobehon100($arr)." số nhỏ hơn 100\n";
            $kq.="Tổng các số âm trong mảng là:".Tongsoam($arr)."\n";
            $kq.=invitri0($arr)."\n";
            sapxeptang($n,$arr);
            $kq.="Sắp xếp theo thứ tự tăng dần:\n".xuatmang($arr);
        }
        else $kq=$n." không phải là số nguyên dương";
    }
    ?>
    <form action="form.php" method="Post">
        <table align="center" bgcolor="#FAFAD2" id="box">
            <tr >
                <td colspan="3"  align="center" bgcolor="#FFFF00">
                    <h2 style="color:red">Bài 1</h2>
                </td>
            </tr>
            <tr>
                <td>Nhập vào số </td>
                <td>
                    <input type="text" name="input" size="42" placeholder="nhập vào số nguyên dương"
                    value="<?php if(isset($_POST['input'])) echo $_POST['input'];?>"
                    >
                </td>
                <td></td>
            </tr>
            <tr>
                <td>Kết quả</td>
                <td>
                    <textarea name="ketqua" cols="40" rows="10" ><?php if(isset($_POST['ketqua'])) echo $kq;?></textarea>
                </td>
            </tr>
            <tr>
                <td colspan="3" align="center">
                    <input type="submit" name="submit" value="Thực hiện" >
                </td>
            </tr>
        </table>
    </form>
</body>
</html>
