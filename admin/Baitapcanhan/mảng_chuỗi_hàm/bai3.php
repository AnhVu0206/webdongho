<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Bài 3</title>
    <style>
        #box{
            border-width: 2px;
            border-style: solid;
            border-color: grey;
            box-shadow: 5px 5px 5px black;
        }
        #submit{
            text-align: center;
            background-color: #F0E68C;
            width: 60%;
        }
        #reset{
            background-color: #F0E68C;
            width: 20%;
        }
    </style>
</head>
<body>
<?php
function tao_mang($n,&$arr)
{
    for($i=0;$i<$n;$i++)
    {
        $arr[$i]=rand(0,20);
    }
}
function GTLN($arr)
{
    $max=0;
    for($i=0;$i<count($arr);$i++)
    {
        if($arr[$i]>$max) $max=$arr[$i];
    }
    return $max;
}
function GTNN($arr)
{  
    foreach($arr as $i)
    {
        $min=$i;
        for($i=0;$i<count($arr);$i++)
        {
            if($arr[$i]<$min) $min=$arr[$i];
        }
        return $min;
    }
    
}
function Tongmang($arr)
{
    $t=0;
    for($i=0;$i<count($arr);$i++)
    {
        $t=$t+$arr[$i];
    }
    return $t;
}
if(isset($_POST["submit"]))
{
    $n=$_POST["n"];
    $arr=[];
    tao_mang($n,$arr);
    $mang="".implode(" ",$arr);
    $gtln=GTLN($arr);
    $gtnn=GTNN($arr);
    $tong=Tongmang($arr);
}
if(isset($_POST["reset"]))
{
    $n=$_POST["n"];
    $n="";
}
?>
    <form action="bai3.php" method="post">
        <table  align="center" bgcolor="#DDA0DD" id="box">
            <tr>
                <td colspan="4" align="center" bgcolor="#8B008B">
                    <h2 style="color:white">Phát sinh mảng và tính toán</h2>
                </td>
            </tr>
            <tr>
                <td>Nhập số phần tử:</td>
                <td>
                    <input type="text" name="n" size="30"
                    value ="<?php if(isset($_POST['n'])) echo$_POST['n']; ?>"
                    >
                </td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td >
                    <input type="submit" name="submit" id="submit" value="phát sinh và tính toán"> 
                    <input type="submit" name="reset" id="reset" value="reset">
                </td>
            </tr>
            <tr>
                <td>Mảng:</td>
                <td>
                    <input type="text" name="mang" size="40" value="<?php if(isset($_POST['mang'])) echo $mang;?>">
                </td>
                <td>&emsp;</td>
            </tr>
            <tr>
                <td>GTLM (max) trong mảng:</td>
                <td>
                    <input type="text" name="gtln" size="15" value="<?php if(isset($_POST['gtln'])) echo $gtln;?>">
                </td>
                <td>&emsp;</td>
            </tr>
            <tr>
                <td>GTNN (min) trong mảng:</td>
                <td>
                    <input type="text" name="gtnn" size="15" value="<?php if(isset($_POST['gtnn'])) echo $gtnn;?>">
                </td>
                <td>&emsp;</td>
            </tr>
            <tr>
                <td>Tổng mảng:</td>
                <td>
                    <input type="text" name="tong" size="15" value="<?php if(isset($_POST['tong'])) echo $tong;?>">
                </td>
                <td>&emsp;</td>
            </tr>
            <tr>
                <td colspan="3" align="center">
                    <span>(<span style="color:red">Ghi chú:</span>Các phần tử trong mảng có giá trị từ 0 đến 20)</span>
                </td>
                <td>&emsp;</td>
            </tr>
        </table>
    </form>
</body>
</html>