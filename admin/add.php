<?php

session_start();

error_reporting(0);
if (!isset($_GET['id']) && !isset($_GET['loaisp']) && !isset($_GET['icon'])) {
    header('Location: index.php');
} else {
    $maloai =$_GET['id'];
    $tenloai = $_GET['loaisp'];
    $icon = $_GET['icon'];
    $_SESSION['maloai']=$maloai;
    require 'includes/header.php';
    require 'includes/layout.php';
}
?>

<div class="container-fluid product-page">
    <div class="container current-page">
        <nav>
            <div class="nav-wrapper">
                <div class="col s12">
                    <a href="index.php" class="breadcrumb">Quản lý</a>
                    <a href="themsp.php" class="breadcrumb"><?= $tenloai; ?></a>
                </div>
            </div>
        </nav>
    </div>
</div>

<div class="container addp">
    <form method="post" enctype="multipart/form-data" action="action_add.php">
        <div class="card">

            <?php

            include '../config.php';
            ?>
            <div class="center-align">
                <img class="responsive-img" src="src/img/<?= $icon; ?>">
            </div>

            <div class="row">
                <div class="input-field col s6">
                    <i class="fa fa-product-hunt prefix"></i>
                    <input id="icon_prefix" type="text" class="validate" name="tensp">
                    <label for="icon_prefix">Tên sản phẩm</label>
                </div>
                <div class="input-field col s6">
                    <i class="fa fa-cart-plus prefix"></i>
                    <input id="icon_prefix" type="number" class="validate" name="soluong">
                    <label for="icon_prefix">Số lượng</label>
                </div>

                <div class="input-field col s6">
                    <i class="prefix fa fa-money"></i>
                    <input id="icon_prefix" type="number" class="validate" name="gia">
                    <label for="icon_prefix">Giá</label>
                </div>

                <div class="input-field col s6">
                    <select class="icons" name="hangsx" required>
                        <option value="" disabled selected>Chọn hãng sản xuất</option>
                        <?php
                        $query = "SELECT * FROM hangsx ";
                        $result = $conn->query($query);
                        if ($result->num_rows != 0) {
                            while ($row = $result->fetch_array()) {
                                $mahsx = $row['mahsx'];
                                $tenhsx = $row['tenhsx'];
                                ?>
                                    <option value="<?= $mahsx ?>"><?= $tenhsx ?></option>
                                <?php
                            }
                        }
                        ?>
                        
                    </select>
                    <label>Hãng sản xuất</label>
                </div>

                <div class="input-field col s12">
                    <i class="material-icons prefix">mode_edit</i>
                    <textarea id="icon_prefix2" class="materialize-textarea" name="detail"></textarea>
                    <label for="icon_prefix2">Mô tả</label>
                </div>

                <div class="file-field input-field col s6">
                    <div class="btn blue">
                        <span>Avatar</span>
                        <input type="file" name="avatar">
                    </div>
                    <div class="file-path-wrapper">
                        <input class="file-path validate" type="text" name="avatar">
                    </div>
                </div>

                <div class="file-field input-field col s2">
                    <div class="red btn">
                        <span>1</span>
                        <input type="file" name="picture1">
                    </div>
                    <div class="file-path-wrapper">
                        <input class="file-path validate" type="text" name="picture1">
                    </div>
                </div>

                <div class="file-field input-field col s2">
                    <div class="red btn">
                        <span>2</span>
                        <input type="file" name="picture2">
                    </div>
                    <div class="file-path-wrapper">
                        <input class="file-path validate" type="text" name="picture2">
                    </div>
                </div>

                <div class="file-field input-field col s2">
                    <div class="red btn">
                        <span>3</span>
                        <input type="file" name="picture3">
                    </div>
                    <div class="file-path-wrapper">
                        <input class="file-path validate" type="text" name="picture3">
                    </div>
                </div>
            </div>

            <div class="center-align">
                <button type="submit" name="done" class="waves-effect button-rounded waves-light btn">Thêm</button>
            </div>
        </div>
        
    </form>
</div>

<?php require 'includes/footer.php'; ?>