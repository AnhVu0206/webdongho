<?php

session_start();

require 'includes/header.php';
require 'includes/layout.php';
?>

<div class="container-fluid product-page">
    <div class="container current-page">
        <nav>
            <div class="nav-wrapper">
                <div class="col s12">
                    <a href="index.php" class="breadcrumb">Watchshop</a>
                    <a href="btcn.php" class="breadcrumb">BTCN</a>
                </div>
            </div>
        </nav>
    </div>
</div>

<div class="container">
    <div class="row">
        <div class="col s12 m4">
            <div class="card">
                <div class="card-image">
                    <img src="src/img/form.png" alt="">
                </div>
                <div class="card-action">
                    <a class="blue-text" href="form.php">Form</a>
                </div>
            </div>
        </div>

        <div class="col s12 m4">
            <div class="card">
                <div class="card-image">
                    <img src="src/img/mang.png" alt="">
                </div>
                <div class="card-action">
                    <a class="blue-text" href="mang.php">Mảng_Chuỗi_Hàm</a>
                </div>
            </div>
        </div>

        <div class="col s12 m4">
            <div class="card">
                <div class="card-image">
                    <img src="src/img/php.png" alt="">
                </div>
                <div class="card-action">
                    <a class="blue-text" href="php.php">MySQL + php</a>
                </div>
            </div>
        </div>
    </div>
</div>


<?php require 'includes/footer.php'; ?>