<?php

session_start();



require 'includes/header.php';
require 'includes/layout.php';
?>

<div class="container-fluid product-page">
    <div class="container current-page">
        <nav>
            <div class="nav-wrapper">
                <div class="col s12">
                    <a href="index.php" class="breadcrumb">Quản lý</a>
                    <a href="ql_sp.php" class="breadcrumb">Quản lý sản phẩm</a>
                    <a href="dondh.php" class="breadcrumb">Đơn đặt hàng</a>
                </div>
            </div>
        </nav>
    </div>
</div>


<div class="container scroll">
    <table class="highlight striped">
        <thead>
            <tr>
                <th data-field="tensp">Tên sản phẩm</th>
                <th data-field="gia_sale">Giá</th>
                <th data-field="soluong">Số lượng</th>
                <th data-field="makh">Khách hàng</th>
                <th data-field="tinhtrang">Tình trạng</th>
                <th data-field="delete">Xoá</th>
                <th data-field="xem">Xem</th>
            </tr>
        </thead>
        <tbody>
            <?php
            include '../config.php';

            $queryfirst = "SELECT
            sanpham.masp as 'masp',
            sanpham.tensp as 'tensp',
            sanpham.gia_sale as 'gia_sale',

            SUM(dondathang.soluong),
            dondathang.tinhtrang as 'tinhtrang',
            dondathang.masp,
            dondathang.soluong as 'soluong',
            dondathang.makh as 'makh'


            FROM sanpham, dondathang
            WHERE sanpham.masp = dondathang.masp AND dondathang.tinhtrang = 'Đã đặt hàng'
            GROUP BY dondathang.id
            ORDER BY SUM(dondathang.makh) DESC ";
            $resultfirst = $conn->query($queryfirst);
            if ($resultfirst->num_rows > 0) {
                // output data of each row
                while ($rowfirst = $resultfirst->fetch_assoc()) {

                    $masp = $rowfirst['masp'];
                    $tensp = $rowfirst['tensp'];
                    $tinhtrang = $rowfirst['tinhtrang'];
                    $soluong = $rowfirst['soluong'];
                    $gia = $rowfirst['gia_sale'];
                    $makh = $rowfirst['makh'];

                    //get user name
                    $queryuser = "SELECT tenkh FROM khachhang WHERE makh = '$makh'";
                    $resultuser = $conn->query($queryuser);
                    if ($resultuser->num_rows > 0) {
                        // output data of each row
                        while ($rowuser = $resultuser->fetch_assoc()) {
                            $tenkh = $rowuser['tenkh'];
            ?>
                            <tr>
                                <td><?= $tensp; ?></td>
                                <td><?= $gia; ?></td>
                                <td><?= $soluong; ?></td>
                                <td><?=$tenkh?></td>
                                <td><?= $tinhtrang; ?></td>
                                <td><a href="xoa_ddh.php?id=<?= $masp; ?>&makh=<?= $makh; ?>"><i class="material-icons red-text">close</i></a></td>
                                <td><a href="xem_ddh.php?id=<?= $masp; ?>&makh=<?= $makh; ?>"><i class="material-icons">visibility</i></a></td>
                            </tr>
            <?php }
                    }
                }
            } ?>
        </tbody>
    </table>
</div>

<?php require 'includes/footer.php'; ?>