<?php

session_start();

require 'includes/header.php';
require 'includes/layout.php';
?>

<div class="container-fluid product-page">
    <div class="container current-page">
        <nav>
            <div class="nav-wrapper">
                <div class="col s12">
                    <a href="index.php" class="breadcrumb">Quản lý</a>
                    <a href="hangsx.php" class="breadcrumb">Hãng sản xuất</a>
                </div>
            </div>
        </nav>
    </div>
</div>

<div class="container">
    <div class="row">
        <div class="col s12 m4">
            <div class="card">
                <div class="card-image">
                    <img src="src/img/add.png" alt="">
                </div>
                <div class="card-action">
                    <a class="blue-text" href="them_hsx.php">Thêm hãng sản xuất</a>
                </div>
            </div>
        </div>

        <div class="col s12 m4">
            <div class="card">
                <div class="card-image">
                    <img src="src/img/xem.png" alt="">
                </div>
                <div class="card-action">
                    <a class="blue-text" href="xem_hsx.php">Xem chi tiết</a>
                </div>
            </div>
        </div>

        <div class="col s12 m4">
            <div class="card">
                <div class="card-image">
                    <img src="src/img/xoa.png" alt="">
                </div>
                <div class="card-action">
                    <a class="blue-text" href="xoa_hsx.php">Xoá hãng sản xuất</a>
                </div>
            </div>
        </div>
    </div>
</div>


<?php require 'includes/footer.php'; ?>