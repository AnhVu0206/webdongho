<?php

session_start();

if ($_SESSION['phanquyen'] !== 'admin') {
    header('Location: ../index.php');
}

require 'includes/header.php';
require 'includes/layout.php';
?>

<div class="container-fluid product-page">
    <div class="container current-page">
        <nav>
            <div class="nav-wrapper">
                <div class="col s12">
                    <a href="../index.php" class="breadcrumb">Watchshop</a>
                    <a href="index.php" class="breadcrumb">Quản lý</a>
                </div>
            </div>
        </nav>
    </div>
</div>

<div class="container dashboard">
    <div class="row">
        <div class="col s12 m4">
            <div class="card horizontal">
                <div class="card-image">
                    <img src="src/img/pixel.png" alt="" />
                </div>
                <div class="card-stacked">
                    <div class="card-content">
                        <p>Sản phẩm & Đơn đặt hàng</p>
                    </div>
                    <div class="card-action">
                        <a href="ql_sp.php" class="blue-text">Chi tiết</a>
                    </div>
                </div>
            </div>
        </div>

        <div class="col s12 m4">
            <div class="card horizontal">
                <div class="card-image">
                    <img src="src/img/cat.png" alt="" />
                </div>
                <div class="card-stacked">
                    <div class="card-content">
                        <p>Kho hàng</p>
                    </div>
                    <div class="card-action">
                        <a href="sanpham.php" class="blue-text">Chi tiết</a>
                    </div>
                </div>

            </div>
        </div>

        <div class="col s12 m4">
            <div class="card horizontal">
                <div class="card-image">
                    <img src="src/img/user.png" alt="" />
                </div>
                <div class="card-stacked">
                    <div class="card-content">
                        <p>Khách hàng</p>
                    </div>
                    <div class="card-action">
                        <a href="khachhang.php" class="blue-text">Chi tiết</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="col s12 m4">
            <div class="card horizontal">
                <div class="card-image">
                    <img src="src/img/hangsx.png" alt="" />
                </div>
                <div class="card-stacked">
                    <div class="card-content">
                        <p>Hãng sản xuất</p>
                    </div>
                    <div class="card-action">
                        <a href="hangsx.php" class="blue-text">Chi tiết</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="col s12 m4">
            <div class="card horizontal">
                <div class="card-image">
                    <img src="src/img/loaisp.png" alt="" />
                </div>
                <div class="card-stacked">
                    <div class="card-content">
                        <p>Loại sản phẩm</p>
                    </div>
                    <div class="card-action">
                        <a href="loaisp.php" class="blue-text">Chi tiết</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="col s12 m4">
            <div class="card horizontal">
                <div class="card-image">
                    <img src="src/img/account.png" alt="" />
                </div>
                <div class="card-stacked">
                    <div class="card-content">
                        <p>Tài khoản </p>
                    </div>
                    <div class="card-action">
                        <a href="taikhoang.php" class="blue-text">Chi tiết</a>
                    </div>
                </div>
            </div>
        </div>
        <?php

        include '../config.php';
        //get total users
        $queryusers = "SELECT count(makh) as total FROM khachhang";
        $resultusers = $conn->query($queryusers);

        if ($resultusers->num_rows > 0) {
            while ($rowusers = $resultusers->fetch_assoc()) {
                $totalusers = $rowusers['total'];
            }
        }

        //get total ordered commands
        $queryorder = "SELECT count(id) as total, tinhtrang FROM dondathang WHERE tinhtrang = 'Đã đặt hàng'";
        $resultorder = $conn->query($queryorder);

        if ($resultorder->num_rows > 0) {
            while ($roworder = $resultorder->fetch_assoc()) {
                $totalorder = $roworder['total'];
            }
        }

        //get total paid commands
        $querypaid = "SELECT count(id) as total, tinhtrang FROM dondathang WHERE tinhtrang = 'Đã thanh toán'";
        $resultpaid = $conn->query($querypaid);

        if ($resultorder->num_rows > 0) {
            while ($rowpaid = $resultpaid->fetch_assoc()) {
                $totalpaid = $rowpaid['total'];
            }
        }
        ?>
        <div class="col s12 m4">
            <div class="card horizontal red lighten-1">
                <div class="card-stacked">
                    <div class="card-content">
                        <h5 class="white-text"><i class="material-icons left">supervisor_account</i> Tổng khách hàng</h5>
                    </div>
                    <div class="card-action">
                        <h5 class="white-text"><?= $totalusers; ?></h5>
                    </div>
                </div>
            </div>
        </div>

        <div class="col s12 m4">
            <div class="card blue lighten-1 horizontal">
                <div class="card-stacked">
                    <div class="card-content">
                        <a href="duyet.php"><h5 class="white-text"><i class="material-icons left">shopping_cart</i>Tổng số đơn đặt hàng</h5></a>
                    </div>
                    <div class="card-action">
                        <h5 class="white-text"><?= $totalorder; ?></h5>
                    </div>
                </div>
            </div>
        </div>

        <div class="col s12 m4">
            <div class="card green lighten-1 horizontal">
                <div class="card-stacked">
                    <div class="card-content">
                        <h5 class="white-text"><i class="material-icons left">shopping_cart</i>Đơn hàng hoàn tất</h5>
                    </div>
                    <div class="card-action">
                        <h5 class="white-text"><?= $totalpaid; ?></h5>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php require 'includes/footer.php'; ?>