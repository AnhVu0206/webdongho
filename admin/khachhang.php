<?php

session_start();

require 'includes/header.php';
require 'includes/layout.php';
?>

<div class="container-fluid product-page">
    <div class="container current-page">
        <nav>
            <div class="nav-wrapper">
                <div class="col s12">
                    <a href="index.php" class="breadcrumb">Quản lý</a>
                    <a href="users" class="breadcrumb">Khách hàng</a>
                </div>
            </div>
        </nav>
    </div>
</div>

<div class="container scroll">
    <table class="highlight striped">
        <thead>
            <tr>
                <th data-field="makh">Họ tên</th>
                <th data-field="email">Email</th>
                <th data-field="dienthoai">Điện thoại</th>
                <th data-field="thanhpho_tinh">Thành phố/Tỉnh</th>
                <th data-field="diachi">Địa chỉ</th>
                <th data-field="delete">Xoá</th>
            </tr>
        </thead>
        <tbody>
            <?php
            include '../config.php';

            //get users
            $queryuser = "SELECT makh, email, tenkh, dienthoai, thanhpho_tinh, diachi  FROM khachhang";
            $resultuser = $conn->query($queryuser);
            if ($resultuser->num_rows > 0) {
                // output data of each row
                while ($rowuser = $resultuser->fetch_assoc()) {
                    $makh = $rowuser['makh'];
                    $name = $rowuser['tenkh'];
                    $email = $rowuser['email'];
                    $city = $rowuser['thanhpho_tinh'];
                    $address = $rowuser['diachi'];
                    $phone = $rowuser['dienthoai'];
            ?>
                    <tr>
                        <td><?php echo " $name "  ?></td>
                        <td><?= $email; ?></td>
                        <td><?= $phone; ?></td>
                        <td><?= $city; ?></td>
                        <td><?= $address; ?></td>
                        <td><a href="xoa_kh.php?id=<?= $makh; ?>"><i class="material-icons red-text">close</i></a></td>
                    </tr>
            <?php }
            }  ?>
        </tbody>
    </table>
</div>

<?php require 'includes/footer.php'; ?>