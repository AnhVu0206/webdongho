<?php

session_start();

$maloai = $_GET['id'];

require 'includes/header.php';
require 'includes/layout.php';; ?>

<div class="container-fluid product-page">
    <div class="container current-page">
        <nav>
            <div class="nav-wrapper">
                <div class="col s12">
                    <a href="index.php" class="breadcrumb">Quản lý</a>
                    <a href="sanpham.php" class="breadcrumb">Kho hàng</a>
                    <a href="khohang.php" class="breadcrumb">Sản phẩm</a>
                </div>
            </div>
        </nav>
    </div>
</div>
<div class="container stocki">
    <div class="row">
        <?php
        include '../config.php';
        // get stock
        $query = "SELECT * FROM sanpham WHERE idlsp = '$maloai'";
        $result = $conn->query($query);
        if ($result->num_rows > 0) {
            while ($rows = $result->fetch_assoc()) {
                $masp = $rows['masp'];
                $tensp = $rows['tensp'];
                $avatar = $rows['avatar'];
                $gia = $rows['gia_sale'];

        ?>
                <div class="col s12 m4">
                    <div class="card hoverable animated slideInUp wow">
                        <div class="card-image">
                            <img src="../avatar_sp/<?= $avatar; ?>">
                            <span class="card-title red-text"><?= $tensp; ?></span>
                        </div>
                        <div class="card-content">
                            <h5 class="white-text"><?= $gia;?> VNĐ</h5>
                            <a class="blue-text" href="xoa_sp.php?id=<?= $masp; ?>">Xoá</a>
                        </div>
                    </div>
                </div>

        <?php }
        } ?>
    </div>
</div>
<?php require 'includes/footer.php'; ?>