<?php

session_start();

require 'includes/header.php';
require 'includes/layout.php';
?>

<div class="container-fluid product-page">
    <div class="container current-page">
        <nav>
            <div class="nav-wrapper">
                <div class="col s12">
                    <a href="index.php" class="breadcrumb">Quản lý</a>
                    <a href="ql_sp.php" class="breadcrumb">Quản lý sản phẩm</a>
                </div>
            </div>
        </nav>
    </div>
</div>

<div class="container">
    <div class="row">
        <div class="col s12 m4">
            <div class="card">
                <div class="card-image">
                    <img src="src/img/add.png" alt="">
                </div>
                <div class="card-action">
                    <a class="blue-text" href="themsp.php">Thêm sản phẩm</a>
                </div>
            </div>
        </div>

        <div class="col s12 m4">
            <div class="card">
                <div class="card-image">
                    <img src="src/img/stats.png" alt="">
                </div>
                <div class="card-action">
                    <a class="blue-text" href="thongso.php">Xem thống kê</a>
                </div>
            </div>
        </div>

        <div class="col s12 m4">
            <div class="card">
                <div class="card-image">
                    <img src="src/img/edit.png" alt="">
                </div>
                <div class="card-action">
                    <a class="blue-text" href="dondh.php">Đơn đặt hàng</a>
                </div>
            </div>
        </div>
    </div>
</div>


<?php require 'includes/footer.php'; ?>