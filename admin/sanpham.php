<?php

session_start();


require 'includes/header.php';
require 'includes/layout.php';
?>

<div class="container-fluid product-page">
    <div class="container current-page">
        <nav>
            <div class="nav-wrapper">
                <div class="col s12">
                    <a href="index.php" class="breadcrumb">Quản lý</a>
                    <a href="sanpham.php" class="breadcrumb">Kho hàng</a>
                </div>
            </div>
        </nav>
    </div>
</div>

<div class="container stock">
    <div class="container">
        <div class="row">
            <?php
            include '../config.php';
            $querystock = "SELECT
            sanpham.idlsp as 'loaisp',
            count(sanpham.idlsp) as 'total',

            loaisp.maloai as 'maloai',
            loaisp.tenloai as 'tensp',
            loaisp.icon as 'icon'

            FROM sanpham, loaisp
            WHERE sanpham.idlsp = loaisp.maloai
            GROUP BY loaisp.maloai";
            $resultstock = $conn->query($querystock);
            if ($resultstock->num_rows > 0) {
                while ($rowstock = $resultstock->fetch_assoc()) {
                    $maloai = $rowstock['maloai'];
                    $name = $rowstock['tensp'];
                    $icon = $rowstock['icon'];
                    $total = $rowstock['total'];

            ?>

                    <div class="col s12 m4">
                        <div class="card hoverable animated slideInUp wow">
                            <div class="card-image">
                                <a href="khohang.php?id=<?= $maloai; ?>&loaisp=<?= $name; ?>&icon=<?= $icon; ?>">
                                    <img src="src/img/<?= $icon; ?>" alt=""></a>
                                <span class="card-title blue-text"><?= $name; ?></span>
                            </div>
                            <div class="card-content">
                                <h5 class="white-text"><?= $total; ?></h5>
                            </div>
                        </div>
                    </div>

            <?php }
            } ?>
        </div>
    </div>
</div>
<?php require 'includes/footer.php'; ?>