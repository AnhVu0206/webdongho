<?php
require 'includes/header.php';
?>
<div class="container-fluid product-page">
    <div class="container current-page">
        <nav>
            <div class="nav-wrapper">
                <div class="col s12">
                    <a href="index.php" class="breadcrumb">Quản lý</a>
                    <a href="loaisp.php" class="breadcrumb">Loại sản phẩm</a>
                    <a href="them_lsp.php" class="breadcrumb">Thêm loại sản phẩm</a>
                </div>
            </div>
        </nav>
    </div>
</div>

<div class="container addp">
    <form method="post" enctype="multipart/form-data" action="action_themlsp.php">
        <div class="card">

            <?php

            include '../config.php';
            ?>

            <div class="row">
                <div class="input-field col s6">
                    <i class="fa fa-product-hunt prefix"></i>
                    <input id="icon_prefix" type="text" class="validate" name="tenloai">
                    <label for="icon_prefix">Tên loại sản phẩm</label>
                </div>
    
                <div class="file-field input-field col s6">
                    <div class="btn blue">
                        <span>Icon</span>
                        <input type="file" name="icon">
                    </div>
                    <div class="file-path-wrapper">
                        <input class="file-path validate" type="text" name="icon">
                    </div>
                </div>
            </div>

            <div class="center-align">
                <button type="submit" name="themlsp" class="waves-effect button-rounded waves-light btn">Thêm</button>
            </div>
        </div>
        
    </form>
</div>

<?php require 'includes/footer.php'; ?>