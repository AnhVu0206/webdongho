<?php

session_start();

require 'includes/header.php';
require 'includes/layout.php';
?>

<div class="container-fluid product-page">
    <div class="container current-page">
        <nav>
            <div class="nav-wrapper">
                <div class="col s12">
                    <a href="index.php" class="breadcrumb">Quản lý</a>
                    <a href="tt_sp.php" class="breadcrumb">Quản lý sản phẩm</a>
                    <a href="themsp.php" class="breadcrumb">Thêm sản phẩm</a>
                </div>
            </div>
        </nav>
    </div>
</div>

<div class="container addproduct">
    <div class="container">
        <div class="row">
            <?php
            include '../config.php';

            //get categories
            $querycategory = "SELECT maloai, tenloai, icon  FROM loaisp";
            $total = $conn->query($querycategory);
            if ($total->num_rows > 0) {
                // output data of each row
                while ($rowcategory = $total->fetch_assoc()) {
                    $maloai = $rowcategory['maloai'];
                    $tenloai = $rowcategory['tenloai'];
                    $icon = $rowcategory['icon'];

            ?>

                    <div class="col s12 m6">
                        <div class="card hoverable animated slideInUp wow">
                            <div class="card-image">
                                <a href="add.php?id=<?= $maloai; ?>&loaisp=<?= $tenloai; ?>&icon=<?= $icon; ?>">
                                    <img src="src/img/<?= $icon; ?>" alt=""></a>
                                <span class="card-title blue-text"><?= $tenloai; ?></span>
                            </div>
                        </div>
                    </div>

            <?php }
            } ?>
        </div>
    </div>
</div>

<?php require 'includes/footer.php'; ?>