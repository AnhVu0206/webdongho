<?php

session_start();

require 'includes/header.php';
require 'includes/layout.php';
?>

<div class="container-fluid product-page">
    <div class="container current-page">
        <nav>
            <div class="nav-wrapper">
                <div class="col s12">
                    <a href="index.php" class="breadcrumb">Quản lý</a>
                    <a href="ql_sp.php" class="breadcrumb">Quản lý sản phẩm</a>
                    <a href="thongso.php" class="breadcrumb">Xem thống kê</a>
                </div>
            </div>
        </nav>
    </div>
</div>

<div class="container center-align staaats">
    <div class="row">
        <?php

        include '../config.php';

        $queryfirst = "SELECT

        sanpham.masp as 'masp',
        sanpham.idlsp,

         SUM(dondathang.soluong) as 'total',
         dondathang.tinhtrang,
         dondathang.masp,

         loaisp.tenloai as 'tenloai',
         loaisp.maloai

         FROM sanpham, dondathang, loaisp
         WHERE sanpham.masp = dondathang.masp
         AND dondathang.tinhtrang = 'Đã thanh toán' AND loaisp.maloai = sanpham.idlsp
         GROUP BY loaisp.maloai";
        $resultfirst = $conn->query($queryfirst);
        if ($resultfirst->num_rows > 0) {
            // output data of each row
            while ($rowfirst = $resultfirst->fetch_assoc()) {

                $idp = $rowfirst['masp'];
                $name_best = $rowfirst['tenloai'];
                $totalsold = $rowfirst['total'];
                $percent = ($totalsold);

        ?>

                <div class="col s2">
                    <p class="black-text"><?= $name_best; ?></p>
                    <div class="card red<?= $idp; ?>" style="padding-top:<?= number_format((float)$percent, 2, '.', ''); ?>%">
                        <h5 class="black-text"><?= number_format((float)$percent, 0, '.', '');  ?></h5>
                    </div>
                </div>


        <?php }
        } ?>

    </div>
</div>
<?php require 'includes/footer.php'; ?>