<?php
session_start();

if(isset($_GET['makh']))
{
    $makh=$_GET['makh'];
}
if(isset($_GET['id']))
{
    $masp=$_GET['id'];
}
require 'includes/header.php';
require 'includes/layout.php';

?>

<div class="container print">
    <table>
        <thead>
            <tr>
                <th data-field="tensp">Tên sản phẩm</th>
                <th data-field="soluong">Số lượng</th>
                <th data-field="thanhtien">Giá</th>
                <th data-field="makh">Người dùng</th>
                <th data-field="thanhpho_tinh">Thành phố/Tỉnh</th>
                <th data-field="dienthoai">Điện thoại</th>
                <th data-field="address">Địa chỉ</th>
                <th data-field="httt">Hình thức thanh toán</th>
                <th data-field="htvc">Hình thức vận chuyển</th>
            </tr>
        </thead>
        <tbody class="scroll">
            <?php
            include '../config.php';
            $total = 0;
            $querydetails = "SELECT ctdh.id_ctdh,
            ctdh.id_ddh as 'id_ddh',
            ctdh.tensp as 'tensp',
            ctdh.makh as 'makh',
            ctdh.masp as 'masp',
            ctdh.tenkh as 'tenkh',
            ctdh.soluong as 'soluong',
            ctdh.diachi as 'diachi',
            ctdh.thanhpho_tinh as 'thanhpho_tinh',
            ctdh.thanhtien as 'thanhtien',
            ctdh.hinhthuctt as 'httt',
            ctdh.hinhthucvc as 'htvc',
            khachhang.makh as 'makh',
            khachhang.dienthoai as 'dienthoai'
            FROM ctdh, khachhang
            WHERE khachhang.makh = ctdh.makh AND ctdh.makh = '$makh' AND ctdh.masp = '$masp' AND ctdh.tinhtrang ='Đã đặt hàng'";
            $result = $conn->query($querydetails);
            if ($result->num_rows > 0) {
                // output data of each row
                while ($rowdetails = $result->fetch_assoc()) {
                    $id_details = $rowdetails['id_ctdh'];
                    $product_details = $rowdetails['tensp'];
                    $quantity_details = $rowdetails['soluong'];
                    $price_details = $rowdetails['thanhtien'];
                    $dienthoai=$rowdetails['dienthoai'];
                    $user_details = $rowdetails['tenkh'];
                    $city_details = $rowdetails['thanhpho_tinh'];
                    $address_details = $rowdetails['diachi'];
                    $id_ddh = $rowdetails['id_ddh'];
                    $httt = $rowdetails['httt'];
                    $htvc = $rowdetails['htvc'];
                    $total += $price_details;
            ?>
                    <tr>
                        <td><?= $product_details; ?></td>
                        <td><?= $quantity_details; ?></td>
                        <td><?= $price_details; ?> VNĐ</td>
                        <td><?= $user_details; ?></td>
                        <td><?= $city_details; ?></td>
                        <td><?= $dienthoai; ?></td>
                        <td><?= $address_details; ?></td>
                        <td><?= $httt; ?></td>
                        <td><?= $htvc; ?></td>
                    </tr>
            <?php }
            }
            ?>
            <tr>
                <th>Thành tiền:</th>
                <td style="color:red"><h4><?= $total ?></h4></td>
            </tr>
            <div class="left-align">
                <?php

                $querycmd = "SELECT id FROM dondathang WHERE id = '$id_ddh'";
                $getid = mysqli_query($conn, $querycmd);
                $rowddh = mysqli_fetch_array($getid);
                $idddh = $rowddh['id'];

                ?>
                <h5>Hoá đơn #<?= $idddh; ?></h5>
            </div>
        </tbody>
    </table>
    <form method="post">
        <center>
        <button type="submit" name="done" class="button-rounded waves-effect waves-light btn" >Đã thanh toán</button>
        </center>
        <?php

        if (isset($_POST['done'])) {
            $queryupdate = "UPDATE ctdh SET tinhtrang = 'Hoàn thành' WHERE makh = '$makh' AND tinhtrang = 'Đã đặt hàng' AND masp = '$masp'";
            $queryupdate = mysqli_query($conn, $queryupdate);
            $queryupdate = "UPDATE dondathang SET tinhtrang = 'Đã thanh toán' WHERE makh = '$makh' AND tinhtrang = 'Đã đặt hàng' AND masp = '$masp'";
            $queryupdate = mysqli_query($conn, $queryupdate);
            echo "<meta http-equiv='refresh' content='0;url=index.php' />";
        }
        ?>
    </form>
</div>