<?php

session_start();

require 'includes/header.php';
require 'includes/layout.php';
?>

<div class="container-fluid product-page">
    <div class="container current-page">
        <nav>
            <div class="nav-wrapper">
                <div class="col s12">
                    <a href="index.php" class="breadcrumb">Quản lý</a>
                    <a href="hangsx.php" class="breadcrumb">Hãng sản xuất</a>
                    <a href="xem_hsx.php" class="breadcrumb">Xem chi tiết</a>
                </div>
            </div>
        </nav>
    </div>
</div>

<div class="container addproduct">
    <div class="container">
        <div class="row">
            <?php
            include '../config.php';

            //get categories
            $queryhsx = "SELECT *  FROM hangsx";
            $resulthsx = $conn->query($queryhsx);
            if ($resulthsx->num_rows > 0) {
                // output data of each row
                while ($rowhsx = $resulthsx->fetch_assoc()) {
                    $mahsx = $rowhsx['mahsx'];
                    $tenhsx = $rowhsx['tenhsx'];
                    $iconhsx = $rowhsx['icon']; 
                    $quocgia = $rowhsx['quocgia'];

            ?>

                    <div class="col s12 m4">
                        <div class="card hoverable animated slideInUp wow">
                            <div class="card-image">
                                <a><img src="src/img/<?= $iconhsx; ?>" alt=""></a>
                            </div>
                        </div>
                    </div>

            <?php }
            } ?>
        </div>
    </div>
</div>

<?php require 'includes/footer.php'; ?>