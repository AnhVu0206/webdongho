<?php

session_start();

require 'includes/header.php';
require 'includes/layout.php';; ?>

<div class="container-fluid product-page">
    <div class="container current-page">
        <nav>
            <div class="nav-wrapper">
                <div class="col s12">
                    <a href="index.php" class="breadcrumb">Quản lý</a>
                    <a href="hangsx.php" class="breadcrumb">Hãng sản xuất</a>
                    <a href="xoa_hsx.php" class="breadcrumb">Xoá hãng sản xuất</a>
                </div>
            </div>
        </nav>
    </div>
</div>
<div class="container stocki">
    <div class="row">
        <?php
        include '../config.php';
        // get stock
        $query = "SELECT * FROM hangsx";
        $result = $conn->query($query);
        if ($result->num_rows > 0) {
            while ($rows = $result->fetch_assoc()) {
                $mahsx = $rows['mahsx'];
                $tenhsx = $rows['tenhsx'];
                $icon = $rows['icon'];
                $quocgia = $rows['quocgia'];

        ?>
                <div class="col s12 m4">
                    <div class="card hoverable animated slideInUp wow">
                        <div class="card-image">
                            <img src="src/img/<?= $icon; ?>">
                        </div>
                        <div class="card-content">
                            <a class="blue-text" href="action_xoa_hsx.php?id=<?=$mahsx?>">Xoá</a>
                        </div>
                    </div>
                </div>

        <?php }
        } ?>
    </div>
</div>
<?php require 'includes/footer.php'; ?>