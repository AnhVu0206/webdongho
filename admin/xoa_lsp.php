<?php

session_start();

require 'includes/header.php';
require 'includes/layout.php';; ?>

<div class="container-fluid product-page">
    <div class="container current-page">
        <nav>
            <div class="nav-wrapper">
                <div class="col s12">
                    <a href="index.php" class="breadcrumb">Dashboard</a>
                    <a href="loaisp.php" class="breadcrumb">Loại sản phẩm</a>
                    <a href="xoa_lsp.php" class="breadcrumb">Xoá loại sản phẩm</a>
                </div>
            </div>
        </nav>
    </div>
</div>
<div class="container stocki">
    <div class="row">
        <?php
        include '../config.php';
        // get stock
        $query = "SELECT * FROM loaisp";
        $result = $conn->query($query);
        if ($result->num_rows > 0) {
            while ($rows = $result->fetch_assoc()) {
                $maloai = $rows['maloai'];
                $tenloai = $rows['tenloai'];
                $icon = $rows['icon'];

        ?>
                <div class="col s12 m4">
                    <div class="card hoverable animated slideInUp wow">
                        <div class="card-image">
                            <img src="../src/img/<?= $icon; ?>">
                        </div>
                        <div class="card-content">
                            <a class="blue-text" href="action_xoa_lsp.php?id=<?=$maloai?>">Xoá</a>
                        </div>
                    </div>
                </div>

        <?php }
        } ?>
    </div>
</div>
<?php require 'includes/footer.php'; ?>