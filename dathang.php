<?php
session_start();

if (!isset($_SESSION['makh']) && !isset($_SESSION['item'])) {
    header('Location: login.php');
} elseif ($_SESSION['item'] < 1) {
    header('Location: index.php');
} else {

    require 'includes/layout.php';
    $email_sess = $_SESSION['email'];
    $tp_tinh = $_SESSION['thanhpho_tinh'];
    $name_sess = $_SESSION['tenkh'];
    $phone_sess = $_SESSION['dienthoai'];
    $address_sess = $_SESSION['diachi'];
}

require 'includes/header.php';
?>
<div class="container-fluid product-page">
    <div class="container current-page">
        <nav>
            <div class="nav-wrapper">
                <div class="col s12">
                    <a href="index.php" class="breadcrumb">Trang chủ</a>
                    <a href="giohang.php" class="breadcrumb">Giỏ hàng</a>
                    <a href="dathang.php" class="breadcrumb">Đặt hàng</a>
                </div>
            </div>
        </nav>
    </div>
</div>

<div class="container checkout">
    <div class="card pay">
        <form method="post" action="thanhtoan.php">
            <div class="row">

                <div class="input-field col s6 ">
                    <i class="material-icons prefix">email</i>
                    <input id="icon_prefix" type="text" name="email" value='<?= $email_sess; ?>' class="validate" required>
                    <label for="icon_prefix">Email</label>
                </div>

                <div class="input-field col s6">
                    <i class="material-icons prefix">perm_identity</i>
                    <input id="icon_prefix" type="text" name="tenkh" value='<?= $name_sess; ?>' class="validate" required>
                    <label for="icon_prefix">Tên khách hàng</label>
                </div>

                <div class="input-field col s6">
                    <i class="material-icons prefix">business</i>
                    <input id="icon_prefix" type="text" name="tp_t" value='<?= $tp_tinh; ?>'  class="validate" required>
                    <label for="icon_prefix">Thành phố/Tỉnh</label>
                </div>


                <div class="input-field col s6">
                    <select class="icons" name="hinhthucvc" required>
                        <option value="" disabled selected>Chọn hình thức vận chuyển</option>
                        <option value="Giao hàng tiết kiệm">Vận chuyển thường(giao hàng tiết kiệm)</option>
                        <option value="Giao hàng nhanh">Vận chuyển Nhanh (Giao hàng nhanh)</option>
                    </select>
                    <label>Vận chuyển</label>
                </div>

                <div class="input-field col s6 ">
                    <i class="material-icons prefix">phone </i>
                    <input id="icon_prefix" type="text" name="phone" value='<?= $phone_sess ?>' class="validate" required>
                    <label for="icon_prefix">Số điện thoại</label>
                </div>

                <div class="input-field col s6">
                    <select class="icons" name="hinhthuctt" required>
                        <option value="" disabled selected>Chọn hình thức thanh toán</option>
                        <option value="Momo">Momo</option>
                        <option value="Thẻ tín dụng">Thẻ tính dụng</option>
                        <option value="Thanh toán khi nhận hàng">Thanh toán khi nhận hàng</option>
                    </select>
                    <label>Thanh toán</label>
                </div>

                
                <div class="input-field col s12 ">
                    <i class="material-icons prefix">location_on</i>
                    <input id="icon_prefix" type="text" value='<?= $address_sess; ?>' name="diachi" class="validate" required>
                    <label for="icon_prefix">Địa chỉ</label>
                </div>

                <div class="center-align">
                    <button type="submit" id="confirmed" name="pay" class="btn meh button-rounded waves-effect waves-light ">Đặt hàng</button>
                </div>
            </div>
        </form>
    </div>
</div>

<?php require 'includes/footer.php'; ?>