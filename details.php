<?php
session_start();

$makh = $_SESSION['makh'];
require 'includes/header.php';
?>

<div class="container print">
    <table>
        <thead>
            <tr>
                <th data-field="name">Tên sản phẩm</th>
                <th data-field="quantity">Số lượng</th>
                <th data-field="price">Giá</th>
                <th data-field="user">Người dùng</th>
                <th data-field="city">Thành phố/Tỉnh</th>
                <th data-field="address">Địa chỉ</th>
            </tr>
        </thead>
        <tbody class="scroll">
            <?php
            include 'config.php';
            $total = 0;
            //get detailss
            $querydetails = "SELECT * FROM ctdh WHERE makh = '$makh' AND tinhtrang ='Đã đặt hàng'";
            $result = $conn->query($querydetails);
            if ($result->num_rows > 0) {
                // output data of each row
                while ($rowdetails = $result->fetch_assoc()) {
                    $id_details = $rowdetails['id_ctdh'];
                    $product_details = $rowdetails['tensp'];
                    $quantity_details = $rowdetails['soluong'];
                    $price_details = $rowdetails['thanhtien'];
                    $user_details = $rowdetails['tenkh'];
                    $city_details = $rowdetails['thanhpho_tinh'];
                    $address_details = $rowdetails['diachi'];
                    $id_ddh = $rowdetails['id_ddh'];
                    $total += $price_details;

            ?>
                    <tr>
                        <td><?= $product_details; ?></td>
                        <td><?= $quantity_details; ?></td>
                        <td><?= $price_details; ?> VNĐ</td>
                        <td><?= $user_details; ?></td>
                        <td><?= $city_details; ?></td>
                        <td><?= $address_details; ?></td>
                    </tr>


            <?php }
            }
            ?>
            <tr>
                <td>Thành tiền:</td>
                <td style="color:red"><h4><?= $total ?></h4></td>
            </tr>
            <div class="left-align">
                <?php

                $querycmd = "SELECT id FROM dondathang WHERE id = '$id_ddh'";
                $getid = mysqli_query($conn, $querycmd);
                $rowddh = mysqli_fetch_array($getid);
                $idddh = $rowddh['id'];

                ?>
                <h5>Hoá đơn #<?= $idddh; ?></h5>
            </div>
        </tbody>
    </table>
    <div class="right-align">
        <p>Cảm ơn bạn đã tin tưởng © Watchshop Inc <?= date('Y'); ?></p>
    </div>

    <form method="post">
        <button type="submit" name="done" class="button-rounded waves-effect waves-light btn">Trang chủ</button>
        <!--<button type="submit" name="done2" class="blue waves-effect waves-light btn">
      save as pdf <i class="fa fa-print"></i></button>-->
        <?php

        if (isset($_POST['done'])) {

            echo "<meta http-equiv='refresh' content='0;url=index.php' />";
        }

        ?>
    </form>
</div>

<?php require 'includes/footer.php'; ?>