<?php
session_start();
if (!isset($_SESSION['makh'])) {
    header('Location:login.php');
}
// unset($_SESSION['item']);
require('includes/layout.php');
require('includes/header.php');
?>

<div class="container-fluid product-page">
    <div class="container current-page">
        <nav>
            <div class="nav-wrapper">
                <div class="col s12">
                    <a href="index.php" class="breadcrumb">Trang Chủ</a>
                    <a href="giohang.php" class="breadcrumb">Giỏ hàng</a>
                </div>
            </div>
        </nav>
    </div>
</div>

<div class="container scroll info">
    <table class="highlight">
        <thead>
            <tr>
                <th data-field="tensp">Tên Sản phẩm</th>
                <th data-field="loaisp">Loại sản phẩm</th>
                <th data-field="gia_sale">Giá</th>
                <th data-field="soluong">Số lượng</th>
                <th data-field="total">Thành tiền</th>
            </tr>
        </thead>
        <tbody>
            <?php

            include 'config.php';
            $total=0;
            //get products
            $queryproduct = "SELECT sanpham.tensp as 'tensp',
            sanpham.masp as 'masp', sanpham.gia_sale as 'gia_sale',
            loaisp.tenloai as 'loaisp', dondathang.makh, dondathang.tinhtrang,
            dondathang.soluong as 'soluong'
            FROM loaisp, sanpham, dondathang
            WHERE dondathang.masp = sanpham.masp AND sanpham.idlsp = loaisp.maloai AND dondathang.tinhtrang = 'Đang đặt hàng'";
            $result1 = $conn->query($queryproduct);
            if ($result1->num_rows > 0) {
                // output data of each row
                while ($rowproduct = $result1->fetch_assoc()) {
                    $id_productdb = $rowproduct['masp'];
                    $name_product = $rowproduct['tensp'];
                    $category_product = $rowproduct['loaisp'];
                    $quantity_product = $rowproduct['soluong'];
                    $price_product = $rowproduct['gia_sale'];
                    $gia=$price_product * $quantity_product;
                    $total+=$gia;
            ?>
                    <tr>
                        <td><?= $name_product; ?></td>
                        <td><?= $category_product; ?></td>
                        <td><?= $price_product; ?></td>
                        <td><?= $quantity_product; ?></td>
                        <td><?= $gia ?></td>
                        <td><a href="xoaddh.php?id=<?= $id_productdb; ?>"><i class="material-icons red-text">close</i></a></td>
                    </tr>
            <?php }
            }
            ?>
            <tr>
                <td>Thành tiền:</td>
                <td style="color:red"><h4><?= $total ?></h4></td>
            </tr>
        </tbody>
    </table>
    <div class="right-align">
        <a href="dathang.php" class='btn-large button-rounded waves-effect waves-light'>
            Đặt hàng <i class="material-icons right">payment</i></a>
    </div>
</div>
<?php
require 'includes/secondfooter.php';
require 'includes/footer.php'; ?>