<div class="navbar-fixed">
    <nav class="navblack">
        <div class="nav-wrapper nav-wrapper-2 container white">
            <ul class="left hide-on-med-and-down">
                <li><a href="index.php" class="brand"></a></li>
                <li><a href="index.php" class="dark-text">WatchShop</a></li>

            </ul>
            
            <ul class="center hide-on-large-only">
                <li><a href="index.php" class="dark-text">WatchShop</a></li>

            </ul>
            
            <?php 
            if(!isset($_SESSION['makh']))
            {
                echo "<ul class='right hide-on-med-and-down'>
                <li><a href='index.php' class='dark-text'>Trang Chủ</a></li>
                <li><a href='giohang.php' class='dark-text baskett'><i class='material-icons'>shopping_cart</i>
                <li><a href='login.php' class='waves-effect waves-light btn button-rounded'>Đăng Nhập</a></li>
                <span class='badge"; ?> <?php if (!isset($_SESSION['item']) or $_SESSION['item'] == 0) echo 'hide'; ?>'> <?= $_SESSION['item']; ?> <?php echo "</span></a></li>"; 
            }
            else
            {
                echo "<ul  class='right hide-on-med-and-down'>
                <li><a href='index.php' class='dark-text'>Trang Chủ</a></li>
                <li><a href='giohang.php' class='dark-text baskett'><i class='material-icons'>shopping_cart</i>
                <span class='badge"; ?> <?php if(!isset($_SESSION['item']) OR $_SESSION['item'] == 0) echo 'hide'; ?>'><?= $_SESSION['item']; ?> <?php echo "</span></a></li>
                <li><a> <span style='font-size:18px; color:#90EE90'>"; ?> <?= $_SESSION['tenkh'] ?> <?php echo "</span></a></li>
                <li><a href='sua_ttcn.php'><img class='responsive-img' src='users/default.jpg'></a></li>
                <li><a href='includes/logout.php' class='waves-effect waves-light btn button-rounded'>Đăng xuất</a></li>
                </ul>";
            }
            ?>
        </div>
    </nav>
</div>