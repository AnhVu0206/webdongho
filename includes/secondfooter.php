<footer class="page-footer">
    <div class="container footer-info">
        <div class="row">
            <div class="col s12 l4 ">
                <ul>
                    <li><a class="grey-text text-lighten-3 animated slideInUp wow" href="#top">Lên đầu trang</a></li>
                    <li><a class="grey-text text-lighten-3 animated slideInUp wow" href="index.php">Trang Chủ</a></li>
                </ul>
            </div>
            <div class="col s12 l4 ">
                <ul>
                    <li><a class="grey-text text-lighten-3 animated slideInUp wow" href="login.php">Bạn chưa đăng nhập ?</a></li>
                    <li><a class="grey-text text-lighten-3 animated slideInUp wow" href="loaisp.php">Đồng hồ</a></li>
                    <li><a class="grey-text text-lighten-3 animated slideInUp wow" href="loaisp">Dây đeo</a></li>
                </ul>
            </div>

            <div class="col s12 l4 ">
                <ul>
                    <li><a class="grey-text text-lighten-3 animated slideInUp wow" href="category">Mắt kính</a></li>
                    <li><a class="grey-text text-lighten-3 animated slideInUp wow" href="category">Pin đồng hồ</a></li>   
                    <li><a class="grey-text text-lighten-3 animated slideInUp wow" href="404">Advertise on Watchshop</a></li>
                </ul>
            </div>
        </div>
    </div>
    <div class="footer-copyright">
        <div class="container">
            © <?= Date('Y'); ?> All rights reserved
            <a class="grey-text text-lighten-4 right animated slideInUp wow" href="http://smakosh.com">Made with <i class="fa fa-heartbeat animated pulse infinite red-text"></i> By Ismail Ghallou</a>
        </div>
    </div>
</footer>