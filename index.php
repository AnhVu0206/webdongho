<?php
session_start();

require('includes/header.php');
require ('includes/layout.php');
?>

<div class="container-fluid home" id="top">
    <div class="container search">
        <nav class="animated slideInUp wow">
            <div class="nav-wrapper">
                <form method="GET" action="search.php">
                    <div class="input-field">
                        <input id="search"  type="search" name='searched' required>
                        <label for="search"><i class="material-icons">search</i></label>
                        <i class="material-icons">close</i>
                    </div>
                    <div class="center-align">
                        <button type="submit" name="search" class="blue waves-light miaw waves-effect btn hide">Search</button>
                    </div>
                </form>
            </div>
        </nav>
    </div>
</div>
<div class="container most">
    <div class="row">
        <?php
        require('config.php');
        $query = "SELECT

        sanpham.masp as 'masp',
        sanpham.tensp as 'tensp',
        sanpham.gia_sale as 'gia_sale',
        sanpham.avatar as 'avatar',

        SUM(dondathang.soluong) as 'total',
        dondathang.tinhtrang,
        dondathang.masp

        FROM sanpham, dondathang
        WHERE sanpham.masp = dondathang.masp AND dondathang.tinhtrang = 'Đã thanh toán'
        GROUP BY sanpham.masp
        ORDER BY SUM(dondathang.soluong) DESC LIMIT 6";
        $result = $conn->query($query);
        if (!$result) echo "cau truy van bi sai";
        if ($result->num_rows != 0) {
            while ($row = $result->fetch_assoc()) {
                $id_best = $row['masp'];
                $name_best = $row['tensp'];
                $price_best = $row['gia_sale'];
                $thumbnail_best = $row['avatar'];
                $totalsold = $row['total'];

        ?>
                <div class="col s12 m4">
                    <div class="card hoverable animated slideInUp wow">
                        <div class="card-image">
                            <a href="sanpham.php?id=<?= $id_best;  ?>"><img src="avatar_sp/<?= $thumbnail_best; ?>"></a>
                            <span class="card-title red-text"><?= $name_best; ?></span>
                            <a href="sanpham.php?id=<?= $id_best; ?>" class="btn-floating red halfway-fab waves-effect waves-light right"><i class="material-icons">add</i></a>
                        </div>
                        <div class="card-content">
                            <div class="container">
                                <div class="row">
                                    <div class="col s12">
                                        <p class="white-text"><i class="left fa fa-money"></i> <?= $price_best ; echo " VNĐ"; ?></p>
                                    </div>
                                    <div class="col s4">
                                        <p class="white-text"><i class="left fa fa-shopping-basket"></i> <?= $totalsold; ?></p>
                                    </div>
                                </div>
                            </div>

                        </div>

                    </div>
                </div>

        <?php
            }
        }

        ?>

    </div>
</div>

<div class="container-fluid center-align categories">
    <a href="#category" class="button-rounded btn-large waves-effect waves-light">Các loại sản phẩm</a>
    <div class="container" id="category">
        <div class="row">
            <?php

            //get categories
            $querycategory = "SELECT maloai, tenloai, icon  FROM loaisp";
            $total = $conn->query($querycategory);
            if ($total->num_rows > 0) {
                // output data of each row
                while ($rowcategory = $total->fetch_assoc()) {
                    $id_category = $rowcategory['maloai'];
                    $name_category = $rowcategory['tenloai'];
                    $icon_category = $rowcategory['icon'];
                    $_SESSION['maloai']=$id_category;

            ?>
                    <div class="col s12 m3">
                        <div class="card hoverable animated slideInUp wow">
                            <div class="card-image">
                                <a href="loaisp.php?id=<?= $id_category; ?>"><img src="src/img/<?= $icon_category; ?>" alt=""></a>
                                <span class="card-title black-text"><?= $name_category; ?></span>
                            </div>
                        </div>
                    </div>

            <?php }
            } ?>
        </div>
    </div>
</div>



<?php
require 'includes/secondfooter.php';
require 'includes/footer.php';
?>