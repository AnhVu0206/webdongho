<?php
session_start();

require 'includes/layout.php';
require 'includes/header.php';
$id_category = $_GET['id'];

?>
<div class="container-fluid product-page">
    <div class="container current-page">
        <nav>
            <div class="nav-wrapper">
                <div class="col s12">
                    <a href="index.php" class="breadcrumb">Trang chủ</a>
                    <a href="loaisp.php?id=<?= $id_category; ?>" class="breadcrumb">Các loại sản phẩm</a>
                </div>
            </div>
        </nav>
    </div>
</div>

<div class="container-fluid category-page">
    <div class="row">

        <div class="col s12 m2 center-align cat">
            <div class="collection card">
                <?php

                include 'config.php';

                //get categories
                $querycategory = "SELECT maloai, tenloai FROM loaisp";
                $total = $conn->query($querycategory);
                if ($total->num_rows > 0) {
                    // output data of each row
                    while ($rowcategory = $total->fetch_assoc()) {
                        $id_categorydb = $rowcategory['maloai'];
                        $name_category = $rowcategory['tenloai'];
                ?>
                        <a href="loaisp.php?id=<?= $id_categorydb; ?>" class='collection-item <?php if ($id_categorydb == $id_category) {
                                                                                                    echo "active";
                                                                                                } ?>'><?= $name_category; ?></a>
                <?php }
                } ?>
            </div>
        </div>

        <div class="col s12 m10 ">
            <div class="container content">
                <div class="center-align">
                    <button class="button-rounded btn-large waves-effect waves-light">Sản phẩm</button>
                </div>
                <div class="row">
                    <?php
                    //get products

                    //pages links
                    $page = isset($_GET['page']) ? (int)$_GET['page'] : 1;
                    $perpage = isset($_GET['per-page']) && $_GET['per-page'] <= 16 ? (int)$_GET['per-page'] : 16;

                    $start = ($page > 1) ? ($page * $perpage) - $perpage : 0;

                    $queryproduct = "SELECT SQL_CALC_FOUND_ROWS masp, tensp, gia_sale, id_hinhanh, avatar FROM sanpham WHERE idlsp = '{$id_category}' ORDER BY masp DESC LIMIT {$start}, 16";
                    $result = $conn->query($queryproduct);

                    //pages
                    $total = $conn->query("SELECT FOUND_ROWS() as total")->fetch_assoc()['total'];
                    $pages = ceil($total / $perpage);

                    if ($result->num_rows > 0) {
                        // output data of each row
                        while ($rowproduct = $result->fetch_assoc()) {
                            $id_product = $rowproduct['masp'];
                            $name_product = $rowproduct['tensp'];
                            $price_product = $rowproduct['gia_sale'];
                            $id_pic = $rowproduct['id_hinhanh'];
                            $thumbnail_product = $rowproduct['avatar'];

                    ?>

                            <div class="col s12 m4">
                                <div class="card hoverable animated slideInUp wow">
                                    <div class="card-image">
                                        <a href="sanpham.php?id=<?= $id_product; ?>">
                                            <img src="avatar_sp/<?= $thumbnail_product; ?>"></a>
                                            <span class="card-title red-text" ><?= $name_product; ?></span>
                                        <a href="sanpham.php?id=<?= $id_product; ?>" class="btn-floating halfway-fab waves-effect waves-light right"><i class="material-icons">add</i></a>
                                    </div>
                                    <div class="card-action">
                                        <div class="container-fluid">
                                            <h5 class="white-text"><?= $price_product; ?> VNĐ</h5>
                                        </div>
                                    </div>
                                </div>
                            </div>
                    <?php }
                    } ?>

                </div>
                <div class="center-align animated slideInUp wow">
                    <ul class="pagination <?php if ($total < 15) {
                                                echo "hide";
                                            } ?>">
                        <li class="<?php if ($page == 1) {
                                        echo 'hide';
                                    } ?>"><a href="?page=<?php echo $page - 1; ?>&per-page=15"><i class="material-icons">chevron_left</i></a></li>
                        <?php for ($x = 1; $x <= $pages; $x++) : $y = $x; ?>


                            <li class="waves-effect pagina  <?php if ($page === $x) {
                                                                echo 'active';
                                                            } elseif ($page <  ($x + 1) or $page > ($x + 1)) {
                                                                echo 'hide';
                                                            } ?>"><a href="?page=<?php echo $x; ?>&per-page=15"><?php echo $x; ?></a></li>



                        <?php endfor; ?>
                        <li class="<?php if ($page == $y) {
                                        echo 'hide';
                                    } ?>"><a href="?page=<?php echo $page + 1; ?>&per-page=15"><i class="material-icons">chevron_right</i></a></li>
                    </ul>
                </div>
            </div>
        </div>

    </div>
</div>

<?php
require 'includes/secondfooter.php';
require 'includes/footer.php'; ?>