<?php
session_start();

require 'includes/header.php';
require 'includes/layout.php';
?>

<div class="container-fluid center-align sign">
    <div class="container">

        <div class="row">
            <div class="col s12">
                <ul class="tabs">
                    <li class="tab col s3"><a class="active" href="#test1">Đăng Nhập</a></li>
                    <li class="tab col s3"><a href="#test2">Đăng ký</a></li>
                </ul>
            </div>

            <div class="container forms">
                <div class="row">

                    <div id="test2" class="col s12 left-align">
                        <div class="card">
                            <div class="row">

                                <form class="col s12" method="POST" enctype="multipart/form-data" action="action_dangky.php">
                                    <div class="row">

                                        <div class="input-field col s6">
                                            <i class="material-icons prefix">email</i>
                                            <input id="icon_prefix" type="text" name="email" class="validate" required>
                                            <label for="icon_prefix">Email</label>
                                        </div>

                                        <div class="input-field col s6">
                                            <i class="material-icons prefix">perm_identity</i>
                                            <input id="icon_prefix" type="text" name="hoten" class="validate" required>
                                            <label for="icon_prefix">Họ Tên</label>
                                        </div>

                                        <div class="input-field col s6">
                                            <i class="material-icons prefix">lock</i>
                                            <input id="icon_prefix" type="password" name="password" class="validate value1" required>
                                            <label for="icon_prefix">Password</label>
                                        </div>

                                        <div class="input-field col s6">
                                            <i class="material-icons prefix">lock</i>
                                            <input id="icon_prefix" type="password" name="confirm_pass" class="validate value2" required>
                                            <label for="icon_prefix">Confirm Password</label>
                                        </div>

                                        <div class="input-field col s6">
                                            <i class="material-icons prefix">business</i>
                                            <input id="icon_prefix" type="text" name="thanhpho" class="validate" required>
                                            <label for="icon_prefix">Tỉnh/Thành phố</label>
                                        </div>

                                        <div class="input-field col s6 ">
                                            <i class="material-icons prefix">phone </i>
                                            <input id="icon_prefix" type="text" name="phone" class="validate" required>
                                            <label for="icon_prefix">Số điện thoại</label>
                                        </div>

                                        <div class="input-field col s12 ">
                                            <i class="material-icons prefix">location_on</i>
                                            <input id="icon_prefix" type="text" name="diachi" class="validate" required>
                                            <label for="icon_prefix">Địa chỉ</label>
                                        </div>

                                        <div class="center-align">
                                            <button type="submit" id="confirmed" name="signup" class="btn meh button-rounded waves-effect waves-light ">Đăng Ký</button>
                                        </div>

                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div id="test1" class="col s12 left-align">

                        <div class="card">
                            <div class="row">
                                <form class="col s12" action="action_dangnhap.php" method="POST">

                                    <div class="input-field col s12">
                                        <i class="material-icons prefix">email</i>
                                        <input id="icon_prefix" type="text" name="email" class="validate" required>
                                        <label for="icon_prefix">Email</label>
                                    </div>
                                    <div class="input-field col s12 meh">
                                        <i class="material-icons prefix">lock</i>
                                        <input id="icon_prefix" type="password" name="password" class="validate" required>
                                        <label for="icon_prefix">Password</label>
                                    </div>

                                    <div class="center-align">
                                        <button type="submit" name="login" class="btn button-rounded waves-effect waves-light ">Đăng Nhập</button>
                                    </div>
                                    
                                    <?php
                                    if(isset($_GET['id'])=='1')
                                    {
                                        echo "<div class='center-align'>
                                        <h5 class='red-text'>Nhập sai tài khoản hoặc mật khẩu !</h5>
                                        </div> ";
                                    }
                                    ?>

                                </form>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<?php require 'includes/footer.php'; ?>