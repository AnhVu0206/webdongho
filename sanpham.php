<?php
session_start();
if(!isset($_SESSION['makh']))
{
    header('location:login.php');
}
else $makh = $_SESSION['makh'];

$id_product = $_GET['id'];
require 'includes/header.php';
require 'includes/layout.php'; ?>

<div class="container-fluid product-page" id="top">
    <div class="container current-page">
        <nav>
            <div class="nav-wrapper">
                <div class="col s12">
                    <a href="index.php" class="breadcrumb">Trang Chủ</a>
                    <a href="sanpham.php?id=<? $id_product; ?>" class="breadcrumb">Sản phẩm</a>
                </div>
            </div>
        </nav>
    </div>
</div>


<div class="container-fluid product">
    <div class="container">
        <div class="row">
            <div class="col s12 m6">
                <div class="card">
                    <div class="card-image">
                        <?php

                        include 'config.php';

                        //get products
                        $queryproduct = "SELECT masp, tensp, gia_sale, detail, id_hinhanh,avatar
                        FROM sanpham WHERE masp = '$id_product'";
                        $result1 = $conn->query($queryproduct);
                        if ($result1->num_rows > 0) {
                            // output data of each row
                            while ($rowproduct = $result1->fetch_assoc()) {
                                $id_productdb = $rowproduct['masp'];
                                $name_product = $rowproduct['tensp'];
                                $price_product = $rowproduct['gia_sale'];
                                $id_pic = $rowproduct['id_hinhanh'];
                                $description = $rowproduct['detail'];
                                $thumbnail_product = $rowproduct['avatar'];
                            }
                        }
                        ?>
                        <img class="materialboxed" width="650" src="avatar_sp/<?= $thumbnail_product; ?>" alt="">
                    </div>
                </div>
                <div class="row">
                    <?php
                    $querypic = "SELECT anh, id_sp FROM hinhanh WHERE id_sp = '$id_pic'";
                    $total = $conn->query($querypic);
                    if ($total->num_rows > 0) {
                        while ($rowpic = $total->fetch_assoc()) {
                            $pics = $rowpic['anh'];
                    ?>
                            <div class="col s12 m4">
                                <div class="card hoverable">
                                    <div class="card-image">
                                        <img class="materialboxed" width="650" src="img_sp/<?= $pics; ?>" alt="">
                                    </div>
                                </div>
                            </div>
                    <?php }
                    } ?>
                </div>
            </div>

            <div class="col s12 m6">
                <form method="post">
                    <h2><?= $name_product; ?></h2>
                    <div class="divider"></div>
                    <div class="stuff">
                        <h3 class="woow">Giá tiền</h3>
                        <h5 style="color:red"> <?= $price_product; ?> VNĐ</h5>
                        <p><?= $description; ?></p>
                        <div class="input-field col s12">
                            <i class="material-icons prefix">shopping_basket</i>
                            <input id="icon_prefix" type="number" name="soluong" min="1" class="validate" required>
                            <label for="icon_prefix">Số lượng</label>
                        </div>
                        <?php

                        if (isset($_POST['buy'])) {

                            if (!isset($_SESSION['makh'])) {
                                echo "<meta http-equiv='refresh' content='0;url=http://localhost/DoAn/webdongho/login.php />";
                            }
                            else {
                                $soluong = $_POST['soluong'];
                                include 'config.php';
                                $querybuy = "SELECT * FROM dondathang WHERE masp = '$id_product' AND makh='$makh' AND tinhtrang='Đang đặt hàng'";

                                $resultbuy=$conn->query($querybuy);

                                if($resultbuy->num_rows == 0)
                                {
                                    $q = "INSERT INTO dondathang(masp, soluong,tinhtrang, makh)
                                    VALUES ('$id_product','$soluong','Đang đặt hàng', '$makh')";
                                    $r=$conn->query($q);
                                    if($r ===True)
                                    {
                                        $_SESSION['item'] += 1;
                                        echo "<meta http-equiv='refresh' content='0;url=sanpham.php?id=$id_product' />";
                                    }
                                    else echo "lỗi";
                                }
                                else
                                {
                                    $row=$resultbuy->fetch_assoc();
                                    $sl = $row['soluong'] + $soluong;
                                    $q = "UPDATE dondathang SET soluong='$sl' WHERE masp = '$id_product' AND makh='$makh' ";
                                    $r = $conn->query($q);
                                    if(!$r) echo "truy vấn lỗi";
                                }
                                // if ($resultbuy === TRUE)
                                // {
                                //     $_SESSION['item'] += 1;

                                //     echo "<meta http-equiv='refresh' content='0;url=sanpham.php?id=$id_productdb' />";
                                // }
                                // else
                                // {
                                //     echo "<h5 class='text-red'>Error: " . $querybuy . "</h5><br><br><br>" . $conn->error;
                                // }
                                $conn->close();
                            }
                            
                        }

                        ?>

                        <div class="center-align">
                            <button type="submit" name="buy" class="btn-large meh button-rounded waves-effect waves-light ">Thêm vào giỏ hàng</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<?php
require 'includes/secondfooter.php';
require 'includes/footer.php'; ?>