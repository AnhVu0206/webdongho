<?php
session_start();

$makh = $_SESSION['makh'];
if (!isset($_GET['search']))
{
    header('Location: index.php');
}
else{
    $word = $_GET['searched'];
}
require 'includes/layout.php';
require 'includes/header.php';
?>

<div class="container-fluid product-page" id="top">
    <div class="container current-page">
        <nav>
            <div class="nav-wrapper">
                <div class="col s12">
                    <a href="index.php" class="breadcrumb">Trang chủ</a>
                    <a href="search.php" class="breadcrumb">Search</a>
                </div>
            </div>
        </nav>
    </div>
</div>

<div class="container search-page">
    <div class="row">
        <?php

        include 'config.php';

        $page = isset($_GET['page']) ? (int)$_GET['page'] : 1;
        $perpage = isset($_GET['per-page']) && $_GET['per-page'] <= 16 ? (int)$_GET['per-page'] : 16;

        $start = ($page > 1) ? ($page * $perpage) - $perpage : 0;

        $queryproduct = "SELECT SQL_CALC_FOUND_ROWS masp, tensp, gia_sale, id_hinhanh, avatar FROM sanpham WHERE tensp LIKE '%$word%' ORDER BY masp DESC LIMIT {$start}, 16";
        $result = $conn->query($queryproduct);

        //pages
        $total = $conn->query("SELECT FOUND_ROWS() as total")->fetch_assoc()['total'];
        $pages = ceil($total / $perpage);

        if ($result->num_rows > 0) {
            // output data of each row
            while ($rowproduct = $result->fetch_assoc()) {
                $id_product = $rowproduct['masp'];
                $name_product = $rowproduct['tensp'];
                $price_product = $rowproduct['gia_sale'];
                $id_pic = $rowproduct['id_hinhanh'];
                $thumbnail_product = $rowproduct['avatar'];

        ?>

                <div class="col s12 m4">
                    <div class="card hoverable animated slideInUp wow">
                        <div class="card-image">
                            <a href="sanpham.php?id=<?= $id_product; ?>">
                                <img src="avatar_sp/<?= $thumbnail_product; ?>"></a>
                            <span class="card-title blue-text"><?= $name_product; ?></span>
                            <a href="sanpham.php?id=<?= $id_product; ?>" class="btn-floating halfway-fab waves-effect waves-light right"><i class="material-icons">add</i></a>
                        </div>
                        <div class="card-action">
                            <div class="container-fluid">
                                <h5 class="white-text"><?= $price_product; ?> VNĐ</h5>
                            </div>
                        </div>
                    </div>
                </div>
        <?php }
        } else {
            echo "<div class='container center-align'>
                   <h4 class='black-text'>Không tìm thấy sản phẩm này</h4>
                   </div>";
        } ?>
    </div>

    <div class="center-align animated slideInUp wow">
        <ul class="pagination <?php if ($total < 15) {
                                    echo "hide";
                                } ?>">
            <li class="<?php if ($page == 1) {
                            echo 'hide';
                        } ?>"><a href="?page=<?php echo $page - 1; ?>&per-page=15"><i class="material-icons">chevron_left</i></a></li>
            <?php for ($x = 1; $x <= $pages; $x++) : $y = $x; ?>


                <li class="waves-effect pagina  <?php if ($page === $x) {
                                                    echo 'active';
                                                } elseif ($page <  ($x + 1) or $page > ($x + 1)) {
                                                    echo 'hide';
                                                } ?>"><a href="?page=<?php echo $x; ?>&per-page=15"><?php echo $x; ?></a></li>



            <?php endfor; ?>
            <li class="<?php if ($page == $y) {
                            echo 'hide';
                        } ?>"><a href="?page=<?php echo $page + 1; ?>&per-page=15"><i class="material-icons">chevron_right</i></a></li>
        </ul>
    </div>
</div>

<?php require 'includes/footer.php'; ?>