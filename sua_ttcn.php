<?php

session_start();


$makh = $_SESSION['makh'];
require 'includes/header.php';
require 'includes/layout.php';
?>

<div class="container-fluid product-page">
    <div class="container current-page">
        <nav>
            <div class="nav-wrapper">
                <div class="col s12">
                    <a href="index.php" class="breadcrumb">Watch shop</a>
                    <a href="sua_ttcn" class="breadcrumb">Sửa thông tin cá nhân</a>
                </div>
            </div>
        </nav>
    </div>
</div>

<div class="container editprofile">
    <div class="container">
        <div class="card">
            <div class="row">

                <form class="col s12" method="POST">
                    <div class="row">
                        <div class="input-field col s6">
                            <i class="material-icons prefix">perm_identity</i>
                            <input id="icon_prefix" type="text" name="tenkh" class="validate" required>
                            <label for="icon_prefix">Tên khách hàng</label>
                        </div>

                        <div class="input-field col s6 ">
                            <i class="material-icons prefix">email</i>
                            <input id="icon_prefix" type="text" name="email" class="validate" required>
                            <label for="icon_prefix">Email</label>
                        </div>

                        <div class="input-field col s6">
                            <i class="material-icons prefix">lock</i>
                            <input id="icon_prefix" type="password" name="password" class="validate value1" required>
                            <label for="icon_prefix">New Password</label>
                        </div>

                        <div class="input-field col s6">
                            <i class="material-icons prefix">lock</i>
                            <input id="icon_prefix" type="password" name="confirmation" class="validate value2" required>
                            <label for="icon_prefix">Confirm Password</label>
                        </div>

                        <div class="input-field col s6">
                            <i class="material-icons prefix">business</i>
                            <input id="icon_prefix" type="text" name="tp_t" class="validate" required>
                            <label for="icon_prefix">Thành phố/Tỉnh</label>
                        </div>

                        <div class="input-field col s6 ">
                            <i class="material-icons prefix">phone </i>
                            <input id="icon_prefix" type="text" name="phone" class="validate" required>
                            <label for="icon_prefix">Số điện thoại</label>
                        </div>

                        <div class="input-field col s12 ">
                            <i class="material-icons prefix">location_on</i>
                            <input id="icon_prefix" type="text" name="diachi" class="validate" required>
                            <label for="icon_prefix">Địa chỉ</label>
                        </div>

                        <?php

                        if (isset($_POST['update']))
                        {
                            $newten= $_POST['tenkh'];
                            $newemail = $_POST['email'];
                            $newpassword = $_POST['password'];
                            $newtp_t= $_POST['tp_t'];
                            $newphone= $_POST['phone'];
                            $newdiachi= $_POST['diachi'];
                            

                            include 'config.php';
                            // update info on users Toble
                            $queryupdate = "UPDATE khachhang SET email ='$newemail', password ='$newpassword',dienthoai='$newphone',
                            thanhpho_tinh='$newtp_t',diachi='$newdiachi', tenkh='$newten'
                            WHERE makh='$makh'";
                            $result = $conn->query($queryupdate);

                            echo "<meta http-equiv='refresh' content='0'; url='sua_ttcn.php' />";
                        }

                        ?>
                        <div class="center-align">
                            <button type="submit" id="confirmed" name="update" class="btn meh button-rounded waves-effect waves-light ">Thay đổi</button>
                        </div>

                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<?php require 'includes/footer.php'; ?>