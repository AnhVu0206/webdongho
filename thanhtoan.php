<?php
session_start();

if (!isset($_SESSION['makh']) && !isset($_POST['pay'])) {
    header('Location: login.php');
}

if (isset($_POST['pay'])) {

    include 'config.php';

    $querycmd = "SELECT sanpham.masp as 'masp',
                        sanpham.tensp as 'tensp',
                        sanpham.gia_sale as 'gia_sale',

                        dondathang.id as 'id_ddh',
                        dondathang.masp,
                        dondathang.soluong as 'soluong',
                        dondathang.tinhtrang,
                        dondathang.makh as 'makh',

                        khachhang.makh

                        FROM sanpham, dondathang, khachhang
                        WHERE sanpham.masp = dondathang.masp AND khachhang.makh = dondathang.makh
                        AND dondathang.makh = '{$_SESSION['makh']}' AND dondathang.tinhtrang = 'Đang đặt hàng'";
    $resultcmd = $conn->query($querycmd);
    if ($resultcmd->num_rows > 0) {
        while ($rowcmd = $resultcmd->fetch_assoc()) {
            $masp = $rowcmd['masp'];
            $tensp = $rowcmd['tensp'];
            $soluong = $rowcmd['soluong'];
            $gia = $rowcmd['gia_sale'];
            $id_ddh = $rowcmd['id_ddh'];
            $makh = $rowcmd['makh'];
            $tenkh = $_POST['tenkh'];
            $tp = $_POST['tp_t'];
            $diachi = $_POST['diachi'];
            $httt = $_POST['hinhthuctt'];
            $htvc = $_POST['hinhthucvc'];

            $giatong = $gia * $soluong;

            $query_details = "INSERT INTO ctdh (id_ddh,tensp,makh,tenkh,masp,soluong,thanhtien,diachi,thanhpho_tinh,tinhtrang,hinhthuctt,hinhthucvc)
            VALUES ('$id_ddh','$tensp','$makh','$tenkh','$masp','$soluong','$giatong','$diachi','$tp','Đã đặt hàng','$httt','$htvc')";
            $resultdetails = $conn->query($query_details);
            if (!$resultdetails) echo "lỗi truy vấn";
            //
            $querypay = "UPDATE dondathang SET tinhtrang = 'Đã đặt hàng' WHERE makh = '{$_SESSION['makh']}' AND tinhtrang = 'Đang đặt hàng'";
            $resultpay = mysqli_query($conn, $querypay);

        }
    }
    unset($_SESSION["item"]);
    require  'includes/layout.php';
    // $makh = $_SESSION['makh'];
    // $email = $_SESSION['email'];
    $ten_kh = $_SESSION['tenkh'];
    // $tp = $_SESSION['thanhpho_tinh'];
    // $diachi = $_SESSION['diachi'];
}

require 'includes/header.php';
?>
<div class="container-fluid product-page">
    <div class="container current-page">
        <nav>
            <div class="nav-wrapper">
                <div class="col s12">
                    <a href="index.php" class="breadcrumb">Trang chủ</a>
                    <a href="giohang.php" class="breadcrumb">Giỏ hàng</a>
                    <a href="dathang.php" class="breadcrumb">Đặt hàng</a>
                    <a href="thanhtoan.php" class="breadcrumb">Thank you</a>
                </div>
            </div>
        </nav>
    </div>
</div>

<div class="container thanks">
    <div class="row">
        <div class="col s12 m3">

        </div>

        <div class="col s12 m6">
            <div class="card center-align">
                <div class="card-image">
                    <img src="src/img/thanks.png" class="responsive-img" alt="">
                </div>
                <div class="card-content center-align">
                    <h5>Cảm ơn bạn đã đặt hàng</h5>
                    <p>Đơn hàng của bạn đang được kiểm duyệt :
                    <h5 class="green-text"><?php echo "$ten_kh"  ?></h5>
                    </p>
                </div>
            </div>

            <div class="center-align">
                <a href="details.php" class="button-rounded blue btn waves-effects waves-light">Details</a>
                <a href="index.php" class="button-rounded btn waves-effects waves-light">Trang chủ</a>
            </div>
        </div>
        <div class="col s12 m3">

        </div>
    </div>
</div>

<?php require 'includes/footer.php'; ?>