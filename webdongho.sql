-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Máy chủ: 127.0.0.1
-- Thời gian đã tạo: Th10 07, 2021 lúc 03:20 AM
-- Phiên bản máy phục vụ: 10.4.21-MariaDB
-- Phiên bản PHP: 8.0.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Cơ sở dữ liệu: `webdongho`
--

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `admin`
--

CREATE TABLE `admin` (
  `maad` int(10) NOT NULL,
  `tenad` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `thanhpho_tinh` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `diachi` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `dienthoai` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ngaytao` datetime NOT NULL,
  `phanquyen` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `admin`
--

INSERT INTO `admin` (`maad`, `tenad`, `password`, `email`, `thanhpho_tinh`, `diachi`, `dienthoai`, `ngaytao`, `phanquyen`) VALUES
(1, 'NguyenAnhVu', '12345', 'admin@gmail.com', 'Nha Trang -  Khánh Hoà', '24 Lê Đại Hành', '036261218', '2021-10-21 10:15:33', 'admin');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `ctdh`
--

CREATE TABLE `ctdh` (
  `id_ctdh` int(10) NOT NULL,
  `id_ddh` int(10) NOT NULL,
  `tensp` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `makh` int(10) NOT NULL,
  `tenkh` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `masp` int(10) NOT NULL,
  `soluong` int(10) NOT NULL,
  `diachi` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `thanhpho_tinh` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `thanhtien` int(10) NOT NULL,
  `hinhthucvc` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `hinhthuctt` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tinhtrang` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `ctdh`
--

INSERT INTO `ctdh` (`id_ctdh`, `id_ddh`, `tensp`, `makh`, `tenkh`, `masp`, `soluong`, `diachi`, `thanhpho_tinh`, `thanhtien`, `hinhthucvc`, `hinhthuctt`, `tinhtrang`) VALUES
(4, 78, 'Apple Watch S6 LTE 40mm viền nhôm dây cao su', 20, 'Nguyễn Anh Vũ', 6, 2, '38 Đào Duy Từ', 'Nha Trang -  Khánh Hoà', 23000000, 'Giao hàng tiết kiệm', 'Thanh toán khi nhận hàng', 'Hoàn thành'),
(5, 79, 'Apple Watch S6 LTE 40mm viền nhôm dây cao su', 20, 'Nguyễn Anh Vũ', 6, 1, '38 Đào Duy Từ', 'Nha Trang -  Khánh Hoà', 11500000, 'Giao hàng tiết kiệm', 'Thanh toán khi nhận hàng', 'Hoàn thành'),
(6, 80, ' G-Shock GA-100GBX-1A4 ', 20, 'Nguyễn Anh Vũ', 7, 1, '38 Đào Duy Từ', 'Nha Trang -  Khánh Hoà', 3500000, 'Giao hàng tiết kiệm', 'Thanh toán khi nhận hàng', 'Hoàn thành'),
(7, 85, '\r\nG-SHOCK AW-591GBX-1A4DR', 1, 'Võ Thanh Hiếu', 8, 1, '54 Trần Duy Hưng', 'Ninh Hoà - Khánh Hoà', 2499000, 'Giao hàng tiết kiệm', 'Thanh toán khi nhận hàng', 'Hoàn thành'),
(8, 88, 'Apple Watch S6 LTE 40mm viền nhôm dây cao su', 1, 'Võ Thanh Hiếu', 6, 1, '54 Trần Duy Hưng', 'Ninh Hoà - Khánh Hoà', 11500000, 'Giao hàng tiết kiệm', 'Thanh toán khi nhận hàng', 'Hoàn thành'),
(9, 89, 'Apple Watch S6 LTE 40mm viền nhôm dây cao su', 20, 'Nguyễn Anh Vũ', 6, 1, '38 Đào Duy Từ', 'Nha Trang -  Khánh Hoà', 11500000, 'Giao hàng tiết kiệm', 'Thanh toán khi nhận hàng', 'Hoàn thành'),
(10, 90, '\r\nG-SHOCK AW-591GBX-1A4DR', 20, 'Nguyễn Anh Vũ', 8, 1, '38 Đào Duy Từ', 'Nha Trang -  Khánh Hoà', 2499000, 'Giao hàng tiết kiệm', 'Thanh toán khi nhận hàng', 'Hoàn thành'),
(11, 91, 'Vỉ 5 viên pin CR2032 ', 1, 'Võ Thanh Hiếu', 13, 1, '54 Trần Duy Hưng', 'Ninh Hoà - Khánh Hoà', 50000, 'Giao hàng tiết kiệm', 'Thanh toán khi nhận hàng', 'Hoàn thành'),
(12, 92, '\r\nG-SHOCK AW-591GBX-1A4DR', 20, 'Nguyễn Anh Vũ', 8, 1, '38 Đào Duy Từ', 'Nha Trang -  Khánh Hoà', 2499000, 'Giao hàng tiết kiệm', 'Thanh toán khi nhận hàng', 'Hoàn thành'),
(13, 93, 'Dây Đeo Apple Watch Thép Không Gỉ ', 20, 'Nguyễn Anh Vũ', 12, 1, '38 Đào Duy Từ', 'Nha Trang -  Khánh Hoà', 149000, 'Giao hàng nhanh', 'Thanh toán khi nhận hàng', 'Đã đặt hàng'),
(14, 94, 'Đồng Hồ Rolex 18238 Day Date President', 2, 'Nguyễn Văn B', 2, 1, 'Quận 5 - 27 Lê Lợi', 'Tp.Hồ Chí Minh', 370000000, 'Giao hàng tiết kiệm', 'Thanh toán khi nhận hàng', 'Đã đặt hàng'),
(15, 95, 'Apple Watch SE 40mm viền nhôm dây cao su', 2, 'Nguyễn Văn B', 4, 1, 'Quận 5 - 27 Lê Lợi', 'Tp.Hồ Chí Minh', 8500000, 'Giao hàng tiết kiệm', 'Thanh toán khi nhận hàng', 'Đã đặt hàng');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `dondathang`
--

CREATE TABLE `dondathang` (
  `id` int(10) NOT NULL,
  `masp` int(10) NOT NULL,
  `soluong` int(10) NOT NULL,
  `ngaythanhtoan` timestamp NOT NULL DEFAULT current_timestamp(),
  `tinhtrang` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `makh` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `dondathang`
--

INSERT INTO `dondathang` (`id`, `masp`, `soluong`, `ngaythanhtoan`, `tinhtrang`, `makh`) VALUES
(1, 1, 1, '2021-10-06 15:16:05', 'Đã thanh toán', 1),
(2, 1, 1, '2021-10-04 15:16:48', 'Đã thanh toán', 2),
(3, 6, 1, '2021-10-07 15:17:22', 'Đã thanh toán', 3),
(4, 1, 1, '2021-10-09 15:25:23', 'Đã thanh toán', 5),
(5, 1, 1, '2021-10-04 15:25:23', 'Đã thanh toán', 4),
(6, 7, 2, '2021-09-30 15:25:23', 'Đã thanh toán', 1),
(7, 7, 1, '2021-09-23 15:25:23', 'Đã thanh toán', 2),
(8, 10, 1, '2021-09-29 15:25:23', 'Đã thanh toán', 4),
(9, 6, 1, '2021-10-22 15:31:09', 'Đã thanh toán', 4),
(10, 6, 1, '2021-09-22 15:25:23', 'Đã thanh toán', 3),
(11, 7, 2, '2021-09-18 15:25:23', 'Đã thanh toán', 1),
(12, 2, 1, '2021-09-08 15:25:23', 'Đã thanh toán', 2),
(13, 11, 1, '2021-10-12 15:25:23', 'Đã thanh toán', 5),
(14, 7, 2, '2021-10-13 11:18:32', 'Đã thanh toán', 3),
(15, 6, 1, '2021-10-10 11:20:23', 'Đã thanh toán', 5),
(16, 12, 1, '2021-09-14 13:40:55', 'Đã thanh toán', 1),
(85, 8, 1, '2021-10-29 13:53:49', 'Đã thanh toán', 1),
(88, 6, 1, '2021-10-30 14:09:08', 'Đã thanh toán', 1),
(89, 6, 1, '2021-11-04 08:38:19', 'Đã thanh toán', 20),
(90, 8, 1, '2021-11-04 11:14:41', 'Đã thanh toán', 20),
(91, 13, 1, '2021-11-04 11:34:32', 'Đã thanh toán', 1),
(92, 8, 1, '2021-11-04 12:17:57', 'Đã thanh toán', 20),
(93, 12, 1, '2021-11-04 12:18:32', 'Đã đặt hàng', 20),
(94, 2, 1, '2021-11-04 13:59:48', 'Đã đặt hàng', 2),
(95, 4, 1, '2021-11-04 14:00:16', 'Đã đặt hàng', 2);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `hangsx`
--

CREATE TABLE `hangsx` (
  `mahsx` int(10) NOT NULL,
  `tenhsx` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `icon` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `quocgia` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `hangsx`
--

INSERT INTO `hangsx` (`mahsx`, `tenhsx`, `icon`, `quocgia`) VALUES
(1, 'Rolex', 'rolex.png', 'Thuỵ Sỹ'),
(2, 'Casio', 'casio.png', 'Nhật Bản'),
(3, 'Omega', 'omega.png', 'Thuỵ Sỹ'),
(4, 'Orient', 'orient.png', 'Nhật Bản'),
(5, 'Seiko', 'seiko.png', 'Nhật Bản'),
(6, 'Apple', 'apple.png', 'Mỹ'),
(9, 'Longines', 'longines.png', 'Thuỵ Sỹ');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `hinhanh`
--

CREATE TABLE `hinhanh` (
  `id` int(10) NOT NULL,
  `anh` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `id_sp` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `hinhanh`
--

INSERT INTO `hinhanh` (`id`, `anh`, `id_sp`) VALUES
(1, 'Rolex Oyster Perpetual1.jpg', 1),
(2, 'Rolex Oyster Perpetual2.jpg', 1),
(3, 'Rolex Oyster Perpetual3.jpg', 1),
(4, 'Hồ Rolex 18238 Day Date 1.jpg', 2),
(5, 'Hồ Rolex 18238 Day Date 2.jpg', 2),
(6, 'Hồ Rolex 18238 Day Date 3.jpg', 2),
(7, 'Đồng Hồ Rolex Oyster Perpetual Submariner 1.jpg', 3),
(8, 'Đồng Hồ Rolex Oyster Perpetual Submariner 2.jpg', 3),
(9, 'Đồng Hồ Rolex Oyster Perpetual Submariner 3.jpg', 3),
(10, 'Apple Watch SE 1.jpg', 4),
(11, 'Apple Watch SE 2.jpg', 4),
(12, 'Apple Watch SE 3.jpg', 4),
(13, 'Apple Watch S6 1.jpg', 5),
(14, 'Apple Watch S6 2.jpg', 5),
(15, 'Apple Watch S6 3.jpg', 5),
(16, 'Apple Watch S6 LTE 1.jpg', 6),
(17, 'Apple Watch S6 LTE 2.jpg', 6),
(18, 'Apple Watch S6 LTE 3.jpg', 6),
(19, 'G-Shock GA-100GBX1A4 1.jpg', 7),
(20, 'G-Shock GA-100GBX1A4 2.jpg', 7),
(21, 'G-Shock GA-100GBX1A4 3.jpg', 7),
(22, 'G-SHOCK AW-591GBX-1A4DR 1.jpg', 8),
(23, 'G-SHOCK AW-591GBX-1A4DR 2.jpg', 8),
(24, 'G-SHOCK AW-591GBX-1A4DR 3.jpg', 8),
(25, 'Đồng hồ Casio G-Shock Nam GST-S300BD 1.png', 9),
(26, 'Đồng hồ Casio G-Shock Nam GST-S300BD 2.png', 9),
(27, 'Đồng hồ Casio G-Shock Nam GST-S300BD 3.png', 9),
(28, 'Orient FAA02005D9 1.jpg', 10),
(29, 'Orient FAA02005D9 2.jpg', 10),
(30, 'Orient FAA02005D9 3.jpg', 10),
(31, 'Đồng hồ Nam Orient Star RE-AT0001L00B 1.jpg', 11),
(32, 'Đồng hồ Nam Orient Star RE-AT0001L00B 2.jpg', 11),
(33, 'Đồng hồ Nam Orient Star RE-AT0001L00B 3.jpg', 11),
(34, 'Dây Apple Watch thép không gỉ 1.jpg', 12),
(35, 'Dây Apple Watch thép không gỉ 2.jpg', 12),
(36, 'Dây Apple Watch thép không gỉ 3.jpg', 12),
(71, 'pincasio1.png', 13),
(72, 'pincasio2.png', 13),
(73, 'pincasio3.png', 13);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `khachhang`
--

CREATE TABLE `khachhang` (
  `makh` int(10) NOT NULL,
  `tenkh` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `dienthoai` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `thanhpho_tinh` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `diachi` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ngaytao` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `khachhang`
--

INSERT INTO `khachhang` (`makh`, `tenkh`, `password`, `dienthoai`, `email`, `thanhpho_tinh`, `diachi`, `ngaytao`) VALUES
(1, 'Võ Thanh Hiếu', '12345', '0351254456', 'khachhang1@gmail.com', 'Ninh Hoà - Khánh Hoà', '54 Trần Duy Hưng', '2021-10-21 15:35:10'),
(2, 'Nguyễn Văn B', '12345', '0354526658', 'khachhang2@gmail.com', 'Tp.Hồ Chí Minh', 'Quận 5 - 27 Lê Lợi', '2021-10-20 17:30:02'),
(3, 'Nguyễn Văn C', '12345', '0362547854', 'khachhang3@gmail.com', 'Đà Nẵng', '46 - Lê Đại Hành', '2021-10-18 17:30:02'),
(4, 'Nguyễn Văn D', '12345', '0362136669', 'khachhang4@gmail.com', 'Tuy Hoà - Phú Yên', '115 - Nguyễn Đình chiểu', '2021-10-18 22:23:59'),
(5, 'Nguyễn Văn E', '12345', '0398756458', 'khachhang5@gmail.com', 'Bà Rịa - Vũng Tàu', '65 - Trần Phú', '2021-10-04 22:23:59'),
(20, 'Nguyễn Anh Vũ', '12345', '0365987478', 'khachhang6@gmail.com', 'Nha Trang -  Khánh Hoà', '38 Đào Duy Từ', '0000-00-00 00:00:00'),
(21, 'Lê Nguyễn Việt Hoàng', '12345', '0358569456', 'khachhang7@gmail.com', 'Ninh Hoà - Khánh Hoà', '37 Lý Thường Kiệt', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `loaisp`
--

CREATE TABLE `loaisp` (
  `maloai` int(10) NOT NULL,
  `tenloai` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `icon` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `loaisp`
--

INSERT INTO `loaisp` (`maloai`, `tenloai`, `icon`) VALUES
(1, 'Đồng hồ', 'dongho.png'),
(2, 'Pin đồng hồ', 'pin.png'),
(3, 'Dây đeo', 'daydeo.png'),
(4, 'Kính thời trang', 'kinh.png');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `sanpham`
--

CREATE TABLE `sanpham` (
  `masp` int(10) NOT NULL,
  `idhsx` int(10) NOT NULL,
  `idlsp` int(10) NOT NULL,
  `id_hinhanh` int(10) NOT NULL,
  `tensp` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `avatar` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `detail` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `soluongton` int(10) NOT NULL,
  `gia_goc` int(10) NOT NULL,
  `gia_sale` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `sanpham`
--

INSERT INTO `sanpham` (`masp`, `idhsx`, `idlsp`, `id_hinhanh`, `tensp`, `avatar`, `detail`, `soluongton`, `gia_goc`, `gia_sale`) VALUES
(1, 1, 1, 1, 'Đồng hồ Rolex Oyster Perpetual Day-Date 36mm', 'Rolex Oyster Perpetural.jpg', 'Đồng hồ Rolex Oyster Perpetual Day-Date 36mm sở hữu đai kính rãnh đặc trưng riêng của Rolex. Đồng hồ được tạo ra để dễ dàng xoay vòng bezel và vỏ để đảm bảo khả năng chống nước tối đa nhất giúp tăng cường độ chống thấm nước tuyệt vời nhất.', 10, 1000000000, 928000000),
(2, 1, 1, 2, 'Đồng Hồ Rolex 18238 Day Date President', 'Đồng Hồ Rolex 18238 .jpg', 'Là một sản phẩm vô cùng độc đáo đến từ thương hiệu Rolex khi sử dụng chất liệu vàng để thiết kế gần như toàn bộ chiếc đồng hồ. Với sản phẩm này, các nhà chế tác đã sử dụng chất liệu vàng nguyên khối 18K.\r\n', 10, 400000000, 370000000),
(3, 1, 1, 3, 'Đồng Hồ Rolex Oyster Perpetual Submariner', 'Đồng Hồ Rolex Oyster Perpetual Submariner.jpg', 'The Oyster Perpetual Submariner Date bằng Oystersteel với đai kính Cerachrom đen và mặt số màu đen có vạch dấu giờ phát quang lớn. Đồng hồ Submariner và Subamriner Date thế hệ mới nhất vẫn trung thành với hình mẫu ban đầu ra mắt từ năm 1953.', 10, 420000000, 399000000),
(4, 6, 1, 4, 'Apple Watch SE 40mm viền nhôm dây cao su', 'apple watch SE.jpg', 'Apple Watch SE 40mm viền nhôm dây cao su hồng có khung viền chắc chắn, thiết kế bo tròn 4 góc giúp thao tác vuốt chạm thoải mái hơn. Mặt kính cường lực Ion-X strengthened glass với kích thước 1.57 inch, hiển thị rõ ràng. ', 10, 10000000, 8500000),
(5, 6, 1, 5, 'Apple Watch S6 40mm viền nhôm dây cao su', 'Apple Watch S6.jpg', 'Apple Watch S6 mang đến những nâng cấp hữu ích để hỗ trợ người dùng một cách tối ưu nhất. Nổi bật trong số đó là chip xử lý S6 cải thiện hiệu năng, hệ điều hành watch OS 7 với nhiều tính năng mới hứa hẹn sẽ mang đến những trải nghiệm tốt hơn, thú vị hơn', 10, 11000000, 9500000),
(6, 6, 1, 6, 'Apple Watch S6 LTE 40mm viền nhôm dây cao su', 'Apple Watch S6 LTE.jpg', 'Apple Watch S6 LTE 40mm viền nhôm dây cao su hồng được trang bị khung viền nhôm và mặt kính Ion-X strengthened glass bền bỉ và cứng cáp, chịu lực tốt, cho bạn thoải mái vận động, không ngại những va đập thường ngày.', 10, 12500000, 11500000),
(7, 2, 1, 7, ' G-Shock GA-100GBX-1A4 ', 'G-Shock GA-100GBX-1A4.jpg', 'G-Shock GA-100GBX-1A4 - Đồng Hồ được thiết kế chắc chắn, ngoại hình góc cạnh phù hợp với các bạn trẻ ngày nay, tạo cảm giác cá tính,năng động....', 10, 4000000, 3500000),
(8, 2, 1, 8, '\r\nG-SHOCK AW-591GBX-1A4DR', 'G-SHOCK AW-591GBX-1A4DR.jpg', 'Khỏe khoắn, cuốn hút và dũng mãnh. Chữ G trong từ G-Shock được bắt nguồn từ chữ cái đầu của từ Gravity, nghĩa là không trọng lực. G-Shock được hiểu với khả năng chống va đập, rơi vỡ. Cái tên đã nói rõ về tính năng và thiết kế của chiếc đồng hồ.', 10, 3500000, 2499000),
(9, 2, 1, 9, 'Đồng hồ Casio G-Shock GST-S300BD-1A', 'Đồng hồ Casio G-Shock Nam GST-S300BD.png', 'Giới thiệu biến thể mới nhất cho dòng sản phẩm G-STEEL mang thiết kế G-SHOCK cỡ nhỏ.\r\nMẫu mới này giảm tổng kích cỡ vỏ xuống khoảng 90% so với các mẫu trước đó, trong khi kết hợp cấu trúc bảo vệ lớp và các chức năng hiện có.\r\n', 10, 10000000, 9499000),
(10, 4, 1, 10, 'Đồng hồ Orient FAA02005D9 ', 'Orient FAA02005D9.jpg', 'Đồng hồ Orient đem đến những sản phẩm ấn tượng chinh phục người nhìn một cách nhanh chóng. Đồng hồ Orient với những chất liệu cao cấp bóng bẩy nâng tầm đẳng cấp cho người sở hữu, phù hợp với doanh nhân thành đạt, dân văn phòng hay các giám đốc công ty. ', 10, 5000000, 3999000),
(11, 4, 1, 11, 'Đồng hồ Nam Orient Star RE-AT0001L00B', 'Đồng hồ Nam Orient Star RE-AT0001L00B.jpg', 'Đồng hồ Orient đem đến những sản phẩm ấn tượng chinh phục người nhìn một cách nhanh chóng. Đồng hồ Orient với những chất liệu cao cấp bóng bẩy nâng tầm đẳng cấp cho người sở hữu, phù hợp với doanh nhân thành đạt, dân văn phòng hay các giám đốc công ty.', 10, 13000000, 12399000),
(12, 6, 3, 12, 'Dây Đeo Apple Watch Thép Không Gỉ ', 'Dây Apple Watch thép không gỉ.jpg', 'Dây Đeo Apple Watch Thép Không Gỉ - Khóa Nam Châm dành cho Apple Watch Series 5/4/3/2/1 - hàng phụ kiện', 10, 200000, 149000),
(13, 2, 2, 13, 'Vỉ 5 viên pin CR2032 ', 'pincasio.png', 'Pin Lithium chất lượng cao , sử dụng cho pin đồng hồ, máy đo huyết áp.....', 10, 0, 50000);

--
-- Chỉ mục cho các bảng đã đổ
--

--
-- Chỉ mục cho bảng `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`maad`);

--
-- Chỉ mục cho bảng `ctdh`
--
ALTER TABLE `ctdh`
  ADD PRIMARY KEY (`id_ctdh`),
  ADD KEY `_FK_ctdh_sp` (`masp`),
  ADD KEY `_FK_ctdh_ddh` (`id_ddh`),
  ADD KEY `_FK_ctdh_makh` (`makh`);

--
-- Chỉ mục cho bảng `dondathang`
--
ALTER TABLE `dondathang`
  ADD PRIMARY KEY (`id`),
  ADD KEY `_FK_ddh_sp` (`masp`),
  ADD KEY `_FK_ddh_kh` (`makh`);

--
-- Chỉ mục cho bảng `hangsx`
--
ALTER TABLE `hangsx`
  ADD PRIMARY KEY (`mahsx`);

--
-- Chỉ mục cho bảng `hinhanh`
--
ALTER TABLE `hinhanh`
  ADD PRIMARY KEY (`id`),
  ADD KEY `_FK_hinhanh_sanpham` (`id_sp`);

--
-- Chỉ mục cho bảng `khachhang`
--
ALTER TABLE `khachhang`
  ADD PRIMARY KEY (`makh`);

--
-- Chỉ mục cho bảng `loaisp`
--
ALTER TABLE `loaisp`
  ADD PRIMARY KEY (`maloai`);

--
-- Chỉ mục cho bảng `sanpham`
--
ALTER TABLE `sanpham`
  ADD PRIMARY KEY (`masp`),
  ADD KEY `_FK_hsx_sp` (`idhsx`),
  ADD KEY `_FK_lsp_sp` (`idlsp`);

--
-- AUTO_INCREMENT cho các bảng đã đổ
--

--
-- AUTO_INCREMENT cho bảng `admin`
--
ALTER TABLE `admin`
  MODIFY `maad` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT cho bảng `ctdh`
--
ALTER TABLE `ctdh`
  MODIFY `id_ctdh` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT cho bảng `dondathang`
--
ALTER TABLE `dondathang`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=96;

--
-- AUTO_INCREMENT cho bảng `hangsx`
--
ALTER TABLE `hangsx`
  MODIFY `mahsx` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT cho bảng `hinhanh`
--
ALTER TABLE `hinhanh`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=74;

--
-- AUTO_INCREMENT cho bảng `khachhang`
--
ALTER TABLE `khachhang`
  MODIFY `makh` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT cho bảng `loaisp`
--
ALTER TABLE `loaisp`
  MODIFY `maloai` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- Các ràng buộc cho các bảng đã đổ
--

--
-- Các ràng buộc cho bảng `ctdh`
--
ALTER TABLE `ctdh`
  ADD CONSTRAINT `_FK_ctdh_makh` FOREIGN KEY (`makh`) REFERENCES `khachhang` (`makh`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `_FK_ctdh_sp` FOREIGN KEY (`masp`) REFERENCES `sanpham` (`masp`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Các ràng buộc cho bảng `dondathang`
--
ALTER TABLE `dondathang`
  ADD CONSTRAINT `_FK_ddh_kh` FOREIGN KEY (`makh`) REFERENCES `khachhang` (`makh`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `_FK_ddh_sp` FOREIGN KEY (`masp`) REFERENCES `sanpham` (`masp`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Các ràng buộc cho bảng `hinhanh`
--
ALTER TABLE `hinhanh`
  ADD CONSTRAINT `_FK_hinhanh_sanpham` FOREIGN KEY (`id_sp`) REFERENCES `sanpham` (`masp`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Các ràng buộc cho bảng `sanpham`
--
ALTER TABLE `sanpham`
  ADD CONSTRAINT `_FK_hsx_sp` FOREIGN KEY (`idhsx`) REFERENCES `hangsx` (`mahsx`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `_FK_lsp_sp` FOREIGN KEY (`idlsp`) REFERENCES `loaisp` (`maloai`) ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
